﻿using ICities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Runaurufu.ClimateControl
{
  internal class ModuleHandling
  {
    public string ModuleIdent { internal set; get; }
    public string ModuleName { internal set; get; }
    public IUserMod Mod { internal set; get; }
  }
}
