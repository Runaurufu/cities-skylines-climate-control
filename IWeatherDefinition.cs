﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Runaurufu.ClimateControl
{
  public class WeatherEventsManager
  {
    private static WeatherEventsManager instance;

    public WeatherEventsManager Instance
    {
      get
      {
        if (instance == null)
          instance = new WeatherEventsManager();
        return instance;
      }
    }

  }


  public interface IWeatherEvent
  {

    /// <summary>
    /// 
    /// </summary>
    /// <param name="state"></param>
    void DoEventStuff(WeatherState state);
  }



  /// <summary>
  /// Defines current state of weather.
  /// </summary>



  [Serializable]
  public class WeatherData
  {
    private List<IWeatherEvent> scheduledEvents;
    private List<IWeatherEvent> ongoingEvents;

  }


  public interface IWeatherDefinition
  {
    /// <summary>
    /// Defines if weather should be simulated as only one at given moment.
    /// </summary>
    bool IsExclusive { get; }

    /// <summary>
    /// Special weathers are not populated via normal climate data.
    /// This cover all event and disaster based weathers.
    /// </summary>
    bool IsSpecial { get; }

    bool CanSpawnEvents(ClimateFrameData frame);
  }

  public class WeatherAction
  {
    /// <summary>
    /// When action starts.
    /// </summary>
    public DateTime StartTimeStamp { get; set; }
    /// <summary>
    /// When action ends.
    /// </summary>
    public DateTime EndTimeStamp { get; set; }

    public TimeSpan Duration
    {
      get { return this.EndTimeStamp - this.StartTimeStamp; }
    }
  }
}
