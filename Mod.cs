﻿using System;
using System.Linq;
using ICities;
using System.Collections.Generic;

namespace Runaurufu.ClimateControl
{
  public class Mod : IUserMod
  {
    private List<ModuleHandling> notifiedModules;
    private UIHelperBase modulesBase;
    private static Mod instance;

    public static Mod GetInstance()
    {
      if (instance == null)
        instance = new Mod();
      return instance;
    }

    public string Name
    {
      get
      {
        return "Climate Control";
      }
    }

    public string Description
    {
      get
      {
        return "It is master climate control mod for all your climate alternation needs.";
      }
    }

    public Mod()
    {
      this.notifiedModules = new List<ModuleHandling>();

      if(instance != null)
      {
        this.notifiedModules = instance.notifiedModules;
      }

      instance = this;
    }

    public void OnSettingsUI(UIHelperBase helper)
    {
      GlobalConfig.LoadConfig();
      UIHelperBase helperBase = helper.AddGroup("Main Options");
      helperBase.AddDropdown("Climate Date Time Uses",
        new string[]
        { "Simulation Time", "Sun Time", "Workstation Time", "\"Rush Hour\" Mod Time" },
        ModSettings.CurrentSimulationTimeIndex,
        ModSettings.CurrentSimulationTimeIndexChanged);

      helperBase.AddCheckbox("Chirp forecast", GlobalConfig.GetInstance().ChirpForecast, ModSettings.ChirpForecastOnCheckChanged);
      helperBase.AddCheckbox("SnowDump snow melting when hot", GlobalConfig.GetInstance().AlterSnowDumpSnowMelting, ModSettings.AlterSnowDumpSnowMeltingOnCheckChanged);
      helperBase.AddCheckbox("Random pieces of water will appear on map when raining", GlobalConfig.GetInstance().RainfallMakesWater, ModSettings.RainfallMakesWaterOnCheckChanged);

      helperBase.AddDropdown("Thunders frequency",
        new string[]
        { "Almost Never", "Rarely", "Below Average", "On Average", "Above Average", "Often", "Almost Constantly" },
        ModSettings.ThundersFrequencyIndex,
        ModSettings.ThundersFrequencyIndexChanged);

      //  helperBase.AddCheckbox("Weather alter building fires", GlobalConfig.GetInstance().WeatherAlterFires, ModSettings.WeatherAlterFiresOnCheckChanged);

      helperBase = helper.AddGroup("Allow Alteration");
      helperBase.AddCheckbox("Temperature", GlobalConfig.GetInstance().AlterTemperature, ModSettings.AlterTemperatureOnCheckChanged);
      helperBase.AddCheckbox("Rain", GlobalConfig.GetInstance().AlterRain, ModSettings.AlterRainOnCheckChanged);
      helperBase.AddCheckbox("Fog", GlobalConfig.GetInstance().AlterFog, ModSettings.AlterFogOnCheckChanged);
      helperBase.AddCheckbox("Rainbow", GlobalConfig.GetInstance().AlterRainbow, ModSettings.AlterRainbowOnCheckChanged);
      helperBase.AddCheckbox("Clouds", GlobalConfig.GetInstance().AlterClouds, ModSettings.AlterCloudsOnCheckChanged);
      helperBase.AddCheckbox("Northern Lights", GlobalConfig.GetInstance().AlterNorthernLights, ModSettings.AlterNorthernLightsOnCheckChanged);
      helperBase.AddCheckbox("Ground Wetness", GlobalConfig.GetInstance().AlterGroundWetness, ModSettings.AlterGroundWetnessOnCheckChanged);
      helperBase.AddCheckbox("Wind Direction", GlobalConfig.GetInstance().AlterWindDirection, ModSettings.AlterWindDirectionOnCheckChanged);
      helperBase.AddCheckbox("Day Length", GlobalConfig.GetInstance().AlterDayLength, ModSettings.AlterDayLengthOnCheckChanged);

      helperBase = helper.AddGroup("Climate Presets");
      helperBase.AddDropdown("Climate presets",
        ModSettings.AllPresets.Select(p => p.PresetName).ToArray(),
        ModSettings.CurrentClimatePresetIndex,
        ModSettings.ClimatePresetIndexChanged);

      helperBase.AddDropdown("Climate transition smoothness",
        new string[]
        { "None", "Minimal", "Low", "Below Average", "Average", "Above Average", "High", "Huge", "Full" },
        ModSettings.ClimateSmoothnessIndex,
        ModSettings.ClimateSmoothnessIndexChanged);

      helperBase = helper.AddGroup("Maintenance");
      helperBase.AddButton("Clear save game history data", ModSettings.ClearHistoryData);

      this.modulesBase = helper.AddGroup("Registered Modules");

      ModuleHandling[] modules = ClimateControlEngine.GetInstance().RegisteredModules;
      if (modules.Length > 0)
      {
        foreach (ModuleHandling module in modules)
        {
          this.AddRegisteredModuleToUI(module);
        }

        ModuleHandling[] notifiedModules = this.notifiedModules.Where(i => modules.Any(m => m.ModuleName == i.ModuleName)).ToArray();

        foreach (ModuleHandling module in notifiedModules)
        {
          this.AddNotifiedModuleToUI(module);
        }
      }

      //helperBase.AddButton("SAVE ENV!", Click);
      //helperBase.AddButton("LOAD ENV!", Click2);
    }

    internal void NotifyModule(ModuleHandling module)
    {
      if (this.notifiedModules.Any(m => m.Mod.Name == module.Mod.Name) == false)
      {
        this.notifiedModules.Add(module);
        this.AddNotifiedModuleToUI(module);
      }
    }

    internal void RegisterModule(ModuleHandling module)
    {
      this.AddRegisteredModuleToUI(module);
    }

    private void AddNotifiedModuleToUI(ModuleHandling module)
    {
      if (this.modulesBase == null)
        return;

      this.modulesBase.AddGroup(module.ModuleName + " was notified by " + module.Mod.Name);
    }

    private void AddRegisteredModuleToUI(ModuleHandling module)
    {
      if (this.modulesBase == null)
        return;

      this.modulesBase.AddGroup(module.ModuleName + " was registered by " + module.Mod.Name);
    }

    //private static void Click()
    //{
    //  ClimateControlEngine.Instance.SaveTextures();
    //}

    //private static void Click2()
    //{
    //  ClimateControlEngine.Instance.LoadTextures();
    //}
  }

  internal static partial class ModSettings
  {
    public static void ClearHistoryData()
    {
      ClimateControlEngine.GetInstance().HistoricalData = new HistoryData();
    }

    public static int CurrentSimulationTimeIndex
    {
      get
      {
        return (int)GlobalConfig.GetInstance().TimeToUse;
      }
    }

    public static void CurrentSimulationTimeIndexChanged(int newIndex)
    {
      if (newIndex < 0)
        return;

      GlobalConfig.GetInstance().TimeToUse = (GlobalConfig.TimeType)newIndex;
      GlobalConfig.SaveConfig();
    }

    public static void AlterTemperatureOnCheckChanged(bool isChecked)
    {
      GlobalConfig.GetInstance().AlterTemperature = isChecked;
      GlobalConfig.SaveConfig();

      ClimateControlEngine.GetInstance().InitializeClimateFrames();
    }

    public static void AlterRainOnCheckChanged(bool isChecked)
    {
      GlobalConfig.GetInstance().AlterRain = isChecked;
      GlobalConfig.SaveConfig();

      ClimateControlEngine.GetInstance().InitializeClimateFrames();
    }

    public static void AlterFogOnCheckChanged(bool isChecked)
    {
      GlobalConfig.GetInstance().AlterFog = isChecked;
      GlobalConfig.SaveConfig();

      ClimateControlEngine.GetInstance().InitializeClimateFrames();
    }

    public static void AlterRainbowOnCheckChanged(bool isChecked)
    {
      GlobalConfig.GetInstance().AlterRainbow = isChecked;
      GlobalConfig.SaveConfig();

      ClimateControlEngine.GetInstance().InitializeClimateFrames();
    }

    public static void AlterCloudsOnCheckChanged(bool isChecked)
    {
      GlobalConfig.GetInstance().AlterClouds = isChecked;
      GlobalConfig.SaveConfig();

      ClimateControlEngine.GetInstance().InitializeClimateFrames();
    }

    public static void AlterNorthernLightsOnCheckChanged(bool isChecked)
    {
      GlobalConfig.GetInstance().AlterNorthernLights = isChecked;
      GlobalConfig.SaveConfig();

      ClimateControlEngine.GetInstance().InitializeClimateFrames();
    }

    public static void AlterGroundWetnessOnCheckChanged(bool isChecked)
    {
      GlobalConfig.GetInstance().AlterGroundWetness = isChecked;
      GlobalConfig.SaveConfig();

      ClimateControlEngine.GetInstance().InitializeClimateFrames();
    }

    public static void AlterWindDirectionOnCheckChanged(bool isChecked)
    {
      GlobalConfig.GetInstance().AlterWindDirection = isChecked;
      GlobalConfig.SaveConfig();

      ClimateControlEngine.GetInstance().InitializeClimateFrames();
    }

    public static void AlterDayLengthOnCheckChanged(bool isChecked)
    {
      GlobalConfig.GetInstance().AlterDayLength = isChecked;
      GlobalConfig.SaveConfig();

      ClimateControlEngine.GetInstance().InitializeClimateFrames();
    }

    public static void ChirpForecastOnCheckChanged(bool isChecked)
    {
      GlobalConfig.GetInstance().ChirpForecast = isChecked;
      GlobalConfig.SaveConfig();
    }

    public static void AlterSnowDumpSnowMeltingOnCheckChanged(bool isChecked)
    {
      GlobalConfig.GetInstance().AlterSnowDumpSnowMelting = isChecked;
      GlobalConfig.SaveConfig();
    }

    public static void RainfallMakesWaterOnCheckChanged(bool isChecked)
    {
      GlobalConfig.GetInstance().RainfallMakesWater = isChecked;
      GlobalConfig.SaveConfig();
    }

    public static int ThundersFrequencyIndex
    {
      get
      {
        switch (GlobalConfig.GetInstance().ThundersFrequency)
        {
          case Frequency.AlmostNever:
            return 0;

          case Frequency.Rarely:
            return 1;

          case Frequency.BelowAverage:
            return 2;

          case Frequency.AboveAverage:
            return 4;

          case Frequency.Often:
            return 5;

          case Frequency.AlmostConstantly:
            return 6;

          case Frequency.OnAverage:
          default:
            return 3;
        }
      }
    }

    public static void ThundersFrequencyIndexChanged(int newIndex)
    {
      if (newIndex < 0)
        return;

      switch (newIndex)
      {
        case 0:
          GlobalConfig.GetInstance().ThundersFrequency = Frequency.AlmostNever;
          break;

        case 1:
          GlobalConfig.GetInstance().ThundersFrequency = Frequency.Rarely;
          break;

        case 2:
          GlobalConfig.GetInstance().ThundersFrequency = Frequency.BelowAverage;
          break;

        case 4:
          GlobalConfig.GetInstance().ThundersFrequency = Frequency.AboveAverage;
          break;

        case 5:
          GlobalConfig.GetInstance().ThundersFrequency = Frequency.Often;
          break;

        case 6:
          GlobalConfig.GetInstance().ThundersFrequency = Frequency.AlmostConstantly;
          break;

        case 3:
        default:
          GlobalConfig.GetInstance().ThundersFrequency = Frequency.OnAverage;
          break;
      }
      GlobalConfig.SaveConfig();
      ClimateControlEngine.GetInstance().InitializeConfigValues();
    }

    //public static void WeatherAlterFiresOnCheckChanged(bool isChecked)
    //{
    //  GlobalConfig.GetInstance().WeatherAlterFires = isChecked;
    //  GlobalConfig.SaveConfig();
    //}

    public static int CurrentClimatePresetIndex
    {
      get
      {
        string presetCode = GlobalConfig.GetInstance().SelectedClimatePresetCode;
        if (string.IsNullOrEmpty(presetCode))
          return 0;

        int index = Array.FindIndex(AllPresets, p => p.PresetCode == presetCode);
        if (index == -1)
          return 0;
        return index;
      }
    }

    public static ClimatePreset CurrentClimatePreset
    {
      get
      {
        string presetCode = GlobalConfig.GetInstance().SelectedClimatePresetCode;
        ClimatePreset ret = null;
        if (string.IsNullOrEmpty(presetCode) == false)
          ret = AllPresets.FirstOrDefault(p => p.PresetCode == presetCode);

        if (ret == null)
          ret = AllPresets.First(p => p.PresetCode == DEFAULT_PRESET_CODE);

        return ret;
      }
    }

    public static void ClimatePresetIndexChanged(int newIndex)
    {
      if (newIndex < 0 || newIndex >= AllPresets.Length)
        return;

      ClimatePreset selectedPreset = AllPresets[newIndex];

      GlobalConfig.GetInstance().SelectedClimatePresetCode = selectedPreset.PresetCode;
      GlobalConfig.SaveConfig();

      if (selectedPreset.PresetCode == ModSettings.DEFAULT_PRESET_CODE)
      {
        // Revert to default settings?
        ClimateControlEngine.GetInstance().Initialize(ClimateControlEngine.GetDefaultWeatherProperites());
      }
      else
      {
        ClimateControlEngine.GetInstance().Initialize(selectedPreset.ClimateProperties);
      }
    }

    public static int ClimateSmoothnessIndex
    {
      get
      {
        return (int)GlobalConfig.GetInstance().ClimateSmoothness;
      }
    }

    public static void ClimateSmoothnessIndexChanged(int newIndex)
    {
      if (newIndex < 0)
        return;

      GlobalConfig.GetInstance().ClimateSmoothness = (Level)newIndex;
      GlobalConfig.SaveConfig();

      ClimateControlEngine.GetInstance().InitializeClimateFrames();
    }

    public const string DEFAULT_PRESET_CODE = "__DEFAULT__";
    public const string CUSTOM_PRESET_CODE = "__CUSTOM__";
  }
}