﻿using System;
using System.Collections.Generic;
using System.Reflection;
using ColossalFramework;
using ICities;
using Runaurufu.Utility;
using UnityEngine;

namespace Runaurufu.ClimateControl
{
  public class ChirpBox : IChirperMessage
  {
    private static DateTime lastChrip = DateTime.MinValue;

    public static void SendMessage(string senderName, string msg)
    {
      ChirpPanel cp = ChirpPanel.instance;
      if (cp == null)
        return;

      if (lastChrip == DateTime.MinValue)
        lastChrip = DateTime.Now.AddSeconds(10);

      DateTime now = DateTime.Now;
      //  if ((now - lastChrip).TotalSeconds > 5)
      {
        cp.AddMessage(new ChirpBox() { senderName = senderName, text = msg });
        lastChrip = now;
      }
    }

    public uint senderID
    {
      get
      {
        return 0;
      }
    }

    public string senderName
    {
      get; set;
    }

    public string text
    {
      get; set;
    }
  }

  [Serializable]

  
  public class DefaultSettings
  {
    public Vector3 TerrainManager_GrassFertilityColorOffset { get; set; }
    public Vector3 TerrainManager_GrassFieldColorOffset { get; set; }
    public Vector3 TerrainManager_GrassForestColorOffset { get; set; }
    public Vector3 TerrainManager_GrassPollutionColorOffset { get; set; }
  }

  public class ClimateGlobalValues
  {
    /// <summary>
    /// [mm] of water column.
    /// </summary>
    public float PrecipitationYearlyAverage { get; private set; }
    /// <summary>
    /// [mm] of water column.
    /// </summary>
    public float PrecipitationWeeklyAverage { get; private set; }
    /// <summary>
    /// [mm] of water column.
    /// </summary>
    public float PrecipitationDailyAverage { get; private set; }
    /// <summary>
    /// [mm] of water column.
    /// </summary>
    public float PrecipitationHourlyAverage { get; private set; }

    public static ClimateGlobalValues Create(ClimateFrameData[] climateFrames)
    {
      ClimateGlobalValues ret = new ClimateGlobalValues();

      float precipitationTotal = 0;
      foreach (ClimateFrameData item in climateFrames)
      {
        precipitationTotal += item.PrecipitationAverage * item.YearProgressLength;
      }

      ret.PrecipitationYearlyAverage = precipitationTotal;
      ret.PrecipitationDailyAverage = precipitationTotal / 366f;
      ret.PrecipitationWeeklyAverage = ret.PrecipitationDailyAverage * 7f;
      ret.PrecipitationHourlyAverage = ret.PrecipitationDailyAverage / 24f;

      return ret;
    }
  }

  //  DayNightProperties.instance.m_SunSize = value;
  //  DayNightProperties.instance.m_SunIntensity = value;

  public class TempClimateData
  {
    /// <summary>
    /// [h].
    /// </summary>
    public float FogHoursExpected { get; set; }
    /// <summary>
    /// [h].
    /// </summary>
    public float PrecipitationHoursExpected { get; set; }

    /// <summary>
    /// height of water column gained on area of entire map. [waterCellHeight]
    /// </summary>
    public float PrecipitationAverage { get; set; }
    /// <summary>
    /// height of water column gained on area of entire map. [waterCellHeight]
    /// </summary>
    public float PrecipitationExpected { get; set; }
  }
}