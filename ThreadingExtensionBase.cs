﻿using System;
using ICities;
using Runaurufu.Utility;

namespace Runaurufu.ClimateControl
{
  public class ClimateControlThreading : ThreadingExtensionBase
  {
    public override void OnCreated(IThreading threading)
    {
      base.OnCreated(threading);

      ClimateControlEngine.GetInstance().ThreadingManager = threading;
    }

    public override void OnUpdate(float realTimeDelta, float simulationTimeDelta)
    {
      base.OnUpdate(realTimeDelta, simulationTimeDelta);

      try
      {
        ClimateControlEngine.GetInstance().UpdateClimate(realTimeDelta, simulationTimeDelta);
      }
      catch (Exception ex)
      {
        Logger.Log("UpdateClimate() Exception: " + Environment.NewLine + ex.ToString());
        //Logger.Log(ex.ToString());
      }
    }
  }
}