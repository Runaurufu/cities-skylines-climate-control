﻿using Runaurufu.Utility;
using System;
using System.IO;
using System.Reflection;
using System.Xml;
using System.Xml.Serialization;

namespace Runaurufu.ClimateControl
{
  [Serializable]
  public class GlobalConfig //: IXmlSerializable
  {
    private static GlobalConfig instance;

    public static GlobalConfig GetInstance()
    {
      if (instance == null)
        LoadConfig();

      if (instance == null)
        instance = new GlobalConfig();

      return instance;
    }

    public GlobalConfig()
    {
      this.TimeToUse = TimeType.SunTime;
      this.AlterTemperature = true;
      this.AlterRain = true;
      this.AlterFog = true;
      this.AlterRainbow = true;
      this.AlterClouds = true;
      this.AlterNorthernLights = true;
      this.AlterGroundWetness = true;
      this.AlterWindDirection = true;
      this.AlterDayLength = true;
      this.SelectedClimatePresetCode = ModSettings.DEFAULT_PRESET_CODE;
      this.ChirpForecast = true;
      this.AlterSnowDumpSnowMelting = true;
      this.RainfallMakesWater = false;
      this.ThundersFrequency = Frequency.OnAverage;
      this.ClimateSmoothness = Level.None;
      //  this.WeatherAlterFires = false;
    }

    public string SelectedClimatePresetCode { get; set; }

    public enum TimeType
    {
      /// <summary>
      /// It is simulation time.
      /// </summary>
      SimulationTime = 0,
      /// <summary>
      /// It is day/night cycle time.
      /// </summary>
      SunTime = 1,
      /// <summary>
      /// It is your workstation time.
      /// </summary>
      RealTime = 2,
      /// <summary>
      /// Uses rush hours time (only if available).
      /// </summary>
      RushHours = 3,
    }
    /// <summary>
    /// What type of time should engine use to calculate day time.
    /// </summary>
    public TimeType TimeToUse { get; set; }

    /// <summary>
    /// Should presets affect temperature?
    /// </summary>
    public Boolean AlterTemperature { get; set; }

    /// <summary>
    /// Should presets affect rainfall?
    /// </summary>
    public Boolean AlterRain { get; set; }

    /// <summary>
    /// Should presets affect fog?
    /// </summary>
    public Boolean AlterFog { get; set; }

    /// <summary>
    /// Should presets affect rainbow?
    /// </summary>
    public Boolean AlterRainbow { get; set; }

    /// <summary>
    /// Should presets affect clouds?
    /// </summary>
    public Boolean AlterClouds { get; set; }

    /// <summary>
    /// Should presets affect northern lights?
    /// </summary>
    public Boolean AlterNorthernLights { get; set; }

    /// <summary>
    /// Should presets affect ground wetness?
    /// </summary>
    public Boolean AlterGroundWetness { get; set; }

    /// <summary>
    /// Should presets affect wind direction?
    /// </summary>
    public Boolean AlterWindDirection { get; set; }

    /// <summary>
    /// Should presets affect day length?
    /// </summary>
    public Boolean AlterDayLength { get; set; }

    /// <summary>
    /// Should forecast be chirped?
    /// </summary>
    public Boolean ChirpForecast { get; set; }

    /// <summary>
    /// Alter snowDump snow melting?
    /// </summary>
    public Boolean AlterSnowDumpSnowMelting { get; set; }

    /// <summary>
    /// Does Rainfall spawn random pots of water?
    /// </summary>
    public Boolean RainfallMakesWater { get; set; }

    /// <summary>
    /// How often thunders shall strike?
    /// </summary>
    public Frequency ThundersFrequency { get; set; }

    /// <summary>
    /// How drastic climate changes between frames should be?
    /// </summary>
    public Level ClimateSmoothness { get; set; }

    ///// <summary>
    ///// Should weather change how fires go?
    ///// </summary>
    //public Boolean WeatherAlterFires { get; set; }

    //#region IXmlSerializable
    //public XmlSchema GetSchema()
    //{
    //  return null;
    //}

    //public void ReadXml(System.Xml.XmlReader reader)
    //{
    //  bool wasEmpty = reader.IsEmptyElement;
    //  reader.Read();

    //  if (wasEmpty)
    //    return;

    //  XmlSerializer intSerializer = new XmlSerializer(typeof(int));
    //  XmlSerializer stringSerializer = new XmlSerializer(typeof(string));

    //  reader.ReadStartElement("__version__");
    //  int serializationVersion = (int)intSerializer.Deserialize(reader);
    //  reader.ReadEndElement();

    //  if (serializationVersion >= 1)
    //  {
    //    reader.ReadStartElement("SelectedClimatePresetCode");
    //    SelectedClimatePresetCode = (string)stringSerializer.Deserialize(reader);
    //    reader.ReadEndElement();

    //    reader.ReadStartElement("TimeToUse");
    //    TimeToUse = (TimeType)intSerializer.Deserialize(reader);
    //    reader.ReadEndElement();
    //  }

    //  reader.ReadEndElement();
    //}

    //public void WriteXml(System.Xml.XmlWriter writer)
    //{
    //  XmlSerializer intSerializer = new XmlSerializer(typeof(int));
    //  XmlSerializer stringSerializer = new XmlSerializer(typeof(string));

    //  Int64 serializationVersion = 1;
    //  writer.WriteStartElement("__version__");
    //  intSerializer.Serialize(writer, serializationVersion);
    //  writer.WriteEndElement();

    //  writer.WriteStartElement("SelectedClimatePresetCode");
    //  stringSerializer.Serialize(writer, SelectedClimatePresetCode);
    //  writer.WriteEndElement();

    //  writer.WriteStartElement("TimeToUse");
    //  intSerializer.Serialize(writer, (int)TimeToUse);
    //  writer.WriteEndElement();
    //}
    //#endregion

    public static void LoadConfig()
    {
      if (File.Exists(AssemblyConfig.GetConfigFileName()) == false)
        return;

      try
      {
        XmlSerializer serializer = new XmlSerializer(typeof(GlobalConfig));
        using (XmlReader xmlReader = XmlReader.Create(AssemblyConfig.GetConfigFileName()))
          instance = (GlobalConfig)serializer.Deserialize(xmlReader);

        //using (FileStream stream = File.Open(GlobalConfig.ConfigFileName, FileMode.Open))
        //{
        //  XmlSerializer serializer = new XmlSerializer(typeof(GlobalConfig));
        //  XmlReaderSettings settings = new XmlReaderSettings();
        //  // No settings need modifying here

        //  using (XmlReader xmlReader = XmlReader.Create(stream, settings))
        //  {
        //    return (GlobalConfig)serializer.Deserialize(xmlReader);
        //  }
        //}
      }
      catch (Exception ex)
      {
        instance = new GlobalConfig() { SelectedClimatePresetCode = ModSettings.DEFAULT_PRESET_CODE };
      }
    }

    public static bool SaveConfig()
    {
      if (instance == null)
        return false;

      try
      {
        XmlSerializer serializer = new XmlSerializer(typeof(GlobalConfig));
        using (XmlWriter xmlWriter = XmlWriter.Create(AssemblyConfig.GetConfigFileName()))
          serializer.Serialize(xmlWriter, instance);

        //using (FileStream stream = File.Create(GlobalConfig.ConfigFileName))
        //{
        //  XmlSerializer serializer = new XmlSerializer(typeof(GlobalConfig));
        //  XmlWriterSettings settings = new XmlWriterSettings();
        //  settings.Encoding = new ASCIIEncoding();// new UnicodeEncoding(false, false); // no BOM in a .NET string
        //  settings.Indent = true;
        //  settings.OmitXmlDeclaration = false;

        //  using (XmlWriter xmlWriter = XmlWriter.Create(stream, settings))
        //  {
        //    serializer.Serialize(xmlWriter, instance);
        //  }
        //}
      }
      catch (Exception ex)
      {
        return false;
      }
      return true;
    }
  }
}