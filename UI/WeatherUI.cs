﻿using ColossalFramework;
using ColossalFramework.UI;
using Runaurufu.Utility;
using UnityEngine;

namespace Runaurufu.ClimateControl.UI
{
  internal class WeatherUI : UIPanelAdv
  {
    private ClimateControlEngine ccEngine = null;

    private UILabel dateTimeLabel;

    private UILabel dayTimeLabel;
    private UISliderAdv dayTimeSlider;

    private UILabel temperatureCurrentLabel;
    private UILabel temperatureTargetLabel;

    private UICheckBox temperatreFixedCheckBox;
    private UISliderAdv temperatureCurrentSlider;
    private UISliderAdv temperatureTargetSlider;

    private UILabel rainCurrentLabel;
    private UILabel rainTargetLabel;

    private UICheckBox rainFixedCheckBox;
    private UISliderAdv rainCurrentSlider;
    private UISliderAdv rainTargetSlider;

    private UILabel fogCurrentLabel;
    private UILabel fogTargetLabel;

    private UICheckBox fogFixedCheckBox;
    private UISliderAdv fogCurrentSlider;
    private UISliderAdv fogTargetSlider;

    private UILabel cloudCurrentLabel;
    private UILabel cloudTargetLabel;

    private UICheckBox cloudFixedCheckBox;
    private UISliderAdv cloudCurrentSlider;
    private UISliderAdv cloudTargetSlider;

    //private UILabel rainbowCurrentLabel;
    //private UILabel rainbowTargetLabel;

    //private UICheckBox rainbowFixedCheckBox;
    //private UISliderAdv rainbowCurrentSlider;
    //private UISliderAdv rainbowTargetSlider;

    private UILabel northernLightsCurrentLabel;
    private UILabel northernLightsTargetLabel;

    private UICheckBox northernLightsFixedCheckBox;
    private UISliderAdv northernLightsCurrentSlider;
    private UISliderAdv northernLightsTargetSlider;

    private UILabel groundWetnessLabel;

    private UICheckBox windDirectionFixedCheckBox;
    private UILabel windDirectionLabel;
    private UISliderAdv windDirectionSlider;

    private UILabel seaLevelLabel;
    private UISliderAdv seaLevelSlider;

    public override void Awake()
    {
      this.size = new UnityEngine.Vector2(360f, 950f);
      this.anchor = UIAnchorStyle.Bottom;
      this.backgroundSprite = "ButtonMenu";
      this.autoLayoutPadding = new RectOffset(10, 10, 2, 2);
      this.padding = new RectOffset(0, 0, 0, 5);
      this.autoLayout = true;
      this.autoFitChildrenVertically = true;
      this.autoLayoutDirection = LayoutDirection.Vertical;

      this.dateTimeLabel = this.AddUIComponent<UILabel>();

      this.dayTimeLabel = this.AddUIComponent<UILabel>();
      this.dayTimeSlider = UIFactory.CreateSlider((UIPanel)this, 0f, 24f);
      this.dayTimeSlider.stepSize = 0.0166666675f;
      this.dayTimeSlider.eventValueChanged += DayTimeSlider_eventValueChanged;

      this.temperatureCurrentLabel = this.AddUIComponent<UILabel>();
      UIFactory.CreateCheckBoxWithSlider(this, -240.0f, 600.0f, out this.temperatureCurrentSlider, out this.temperatreFixedCheckBox);
      this.temperatreFixedCheckBox.eventCheckChanged += TemperatuerFixedCheckBox_eventCheckChanged;
      this.temperatureCurrentSlider.stepSize = 0.1f;
      this.temperatureCurrentSlider.eventValueChanged += TemperatureCurrentSlider_eventValueChanged;

      this.temperatureTargetLabel = this.AddUIComponent<UILabel>();
      this.temperatureTargetSlider = UIFactory.CreateSlider((UIPanel)this, -240.0f, 600.0f);
      this.temperatureTargetSlider.stepSize = 0.1f;
      this.temperatureTargetSlider.eventValueChanged += TemperatureTargetSlider_eventValueChanged;

      this.rainCurrentLabel = this.AddUIComponent<UILabel>();
      UIFactory.CreateCheckBoxWithSlider(this, WeatherState.RainMinValue, WeatherState.RainMaxValue, out this.rainCurrentSlider, out this.rainFixedCheckBox);
      this.rainFixedCheckBox.eventCheckChanged += RainFixedCheckBox_eventCheckChanged;
      this.rainCurrentSlider.stepSize = 0.01f;
      this.rainCurrentSlider.eventValueChanged += RainCurrentSlider_eventValueChanged;

      this.rainTargetLabel = this.AddUIComponent<UILabel>();
      this.rainTargetSlider = UIFactory.CreateSlider((UIPanel)this, WeatherState.RainMinValue, WeatherState.RainMaxValue);
      this.rainTargetSlider.stepSize = 0.01f;
      this.rainTargetSlider.eventValueChanged += RainTargetSlider_eventValueChanged;

      this.fogCurrentLabel = this.AddUIComponent<UILabel>();
      UIFactory.CreateCheckBoxWithSlider(this, 0.0f, 1.0f, out this.fogCurrentSlider, out this.fogFixedCheckBox);
      this.fogFixedCheckBox.eventCheckChanged += FogFixedCheckBox_eventCheckChanged;
      this.fogCurrentSlider.stepSize = 0.01f;
      this.fogCurrentSlider.eventValueChanged += FogCurrentSlider_eventValueChanged;

      this.fogTargetLabel = this.AddUIComponent<UILabel>();
      this.fogTargetSlider = UIFactory.CreateSlider((UIPanel)this, 0.0f, 1.0f);
      this.fogTargetSlider.stepSize = 0.01f;
      this.fogTargetSlider.eventValueChanged += FogTargetSlider_eventValueChanged;

      this.cloudCurrentLabel = this.AddUIComponent<UILabel>();
      UIFactory.CreateCheckBoxWithSlider(this, 0.0f, 1.0f, out this.cloudCurrentSlider, out this.cloudFixedCheckBox);
      this.cloudFixedCheckBox.eventCheckChanged += CloudFixedCheckBox_eventCheckChanged;
      this.cloudCurrentSlider.stepSize = 0.01f;
      this.cloudCurrentSlider.eventValueChanged += CloudCurrentSlider_eventValueChanged;

      this.cloudTargetLabel = this.AddUIComponent<UILabel>();
      this.cloudTargetSlider = UIFactory.CreateSlider((UIPanel)this, 0.0f, 1.0f);
      this.cloudTargetSlider.stepSize = 0.01f;
      this.cloudTargetSlider.eventValueChanged += CloudTargetSlider_eventValueChanged;

      //// Rainbow
      //this.rainbowCurrentLabel = this.AddUIComponent<UILabel>();
      //// Vanilla allows 0-1
      //UIFactory.CreateCheckBoxWithSlider(this, WeatherState.RainbowMinValue, WeatherState.RainbowMaxValue, out this.rainbowCurrentSlider, out this.rainbowFixedCheckBox);
      //this.rainbowFixedCheckBox.eventCheckChanged += RainbowFixedCheckBox_eventCheckChanged;
      //this.rainbowCurrentSlider.stepSize = 0.01f;
      //this.rainbowCurrentSlider.eventValueChanged += RainbowCurrentSlider_eventValueChanged;

      //this.rainbowTargetLabel = this.AddUIComponent<UILabel>();
      //this.rainbowTargetSlider = UIFactory.CreateSlider((UIPanel)this, WeatherState.RainbowMinValue, WeatherState.RainbowMaxValue);
      //this.rainbowTargetSlider.stepSize = 0.01f;
      //this.rainbowTargetSlider.eventValueChanged += RainbowTargetSlider_eventValueChanged;

      // Northern Lights
      this.northernLightsCurrentLabel = this.AddUIComponent<UILabel>();
      // Vanilla allows 0-1
      UIFactory.CreateCheckBoxWithSlider(this, WeatherState.NorthernLightsMinValue, WeatherState.NorthernLightsMaxValue, out this.northernLightsCurrentSlider, out this.northernLightsFixedCheckBox);
      this.northernLightsFixedCheckBox.eventCheckChanged += NorthernLightsFixedCheckBox_eventCheckChanged;
      this.northernLightsCurrentSlider.stepSize = 0.1f;
      this.northernLightsCurrentSlider.eventValueChanged += NorthernLightsCurrentSlider_eventValueChanged;

      this.northernLightsTargetLabel = this.AddUIComponent<UILabel>();
      this.northernLightsTargetSlider = UIFactory.CreateSlider((UIPanel)this, WeatherState.NorthernLightsMinValue, WeatherState.NorthernLightsMaxValue);
      this.northernLightsTargetSlider.stepSize = 0.1f;
      this.northernLightsTargetSlider.eventValueChanged += NorthernLightsTargetSlider_eventValueChanged;

      this.groundWetnessLabel = this.AddUIComponent<UILabel>();

      this.windDirectionLabel = this.AddUIComponent<UILabel>();
      UIFactory.CreateCheckBoxWithSlider(this, -180.0f, 180.0f, out this.windDirectionSlider, out this.windDirectionFixedCheckBox);
      this.windDirectionFixedCheckBox.eventCheckChanged += WindDirectionFixedCheckBox_eventCheckChanged;
      this.windDirectionSlider.stepSize = 0.01f;
      this.windDirectionSlider.eventValueChanged += WindDirectionSlider_eventValueChanged;

      this.seaLevelLabel = this.AddUIComponent<UILabel>();
      this.seaLevelSlider = UIFactory.CreateSlider((UIPanel)this, 0, 500);
      this.seaLevelSlider.stepSize = 0.01f;
      this.seaLevelSlider.eventValueChanged += SeaLevelSlider_eventValueChanged;

      this.position = new Vector3(20f, 300f, 0.0f);
      // this.relativePosition = new Vector3(0.0f,  800f);

      this.ccEngine = ClimateControlEngine.GetInstance();

      base.Awake();

      this.StartPosition = Vector3.zero;
    }

    #region FixedCheckBoxes
    private void TemperatuerFixedCheckBox_eventCheckChanged(UIComponent component, bool value)
    {
      if (value)
        ccEngine.FixedTemperature = ccEngine.CurrentTemperature;
      else
        ccEngine.FixedTemperature = null;
    }

    private void WindDirectionFixedCheckBox_eventCheckChanged(UIComponent component, bool value)
    {
      if (value)
        ccEngine.FixedWindDirection = ccEngine.CurrentWindDirection;
      else
        ccEngine.FixedWindDirection = null;
    }

    private void NorthernLightsFixedCheckBox_eventCheckChanged(UIComponent component, bool value)
    {
      if (value)
        ccEngine.FixedNorthernLights = ccEngine.CurrentNorthernLights;
      else
        ccEngine.FixedNorthernLights = null;
    }

    //private void RainbowFixedCheckBox_eventCheckChanged(UIComponent component, bool value)
    //{
    //  if (value)
    //    ccEngine.FixedRainbow = ccEngine.CurrentRainbow;
    //  else
    //    ccEngine.FixedRainbow = null;
    //}

    private void CloudFixedCheckBox_eventCheckChanged(UIComponent component, bool value)
    {
      if (value)
        ccEngine.FixedCloud = ccEngine.CurrentCloud;
      else
        ccEngine.FixedCloud = null;
    }

    private void FogFixedCheckBox_eventCheckChanged(UIComponent component, bool value)
    {
      if (value)
        ccEngine.FixedFog = ccEngine.CurrentFog;
      else
        ccEngine.FixedFog = null;
    }

    private void RainFixedCheckBox_eventCheckChanged(UIComponent component, bool value)
    {
      if (value)
        ccEngine.FixedRain = ccEngine.CurrentRain;
      else
        ccEngine.FixedRain = null;
    }
    #endregion

    private void DayTimeSlider_eventValueChanged(UIComponent component, float value)
    {
      ccEngine.CurrentSolarTimeHour = value;
    }

    private void TemperatureCurrentSlider_eventValueChanged(UIComponent component, float value)
    {
      if (this.temperatreFixedCheckBox.isChecked)
        ccEngine.FixedTemperature = value;
      ccEngine.CurrentTemperature = value;
    }

    private void TemperatureTargetSlider_eventValueChanged(UIComponent component, float value)
    {
      ccEngine.TargetTemperature = value;
    }

    private void TemperatureSpeedSlider_eventValueChanged(UIComponent component, float value)
    {
      ccEngine.TemperatureSpeed = value;
    }

    private void RainCurrentSlider_eventValueChanged(UIComponent component, float value)
    {
      if (this.rainFixedCheckBox.isChecked)
        ccEngine.FixedRain = value;
      ccEngine.CurrentRain = value;
    }

    private void RainTargetSlider_eventValueChanged(UIComponent component, float value)
    {
      ccEngine.TargetRain = value;
    }

    private void WindDirectionSlider_eventValueChanged(UIComponent component, float value)
    {
      if (this.windDirectionFixedCheckBox.isChecked)
        ccEngine.FixedWindDirection = value;
      ccEngine.CurrentWindDirection = value;
    }

    private void SeaLevelSlider_eventValueChanged(UIComponent component, float value)
    {
      ccEngine.SeaLevel = value;
    }

    //private void RainbowTargetSlider_eventValueChanged(UIComponent component, float value)
    //{
    //  ccEngine.TargetRainbow = value;
    //}

    //private void RainbowCurrentSlider_eventValueChanged(UIComponent component, float value)
    //{
    //  if (this.rainbowFixedCheckBox.isChecked)
    //    ccEngine.FixedRainbow = value;
    //  ccEngine.CurrentRainbow = value;
    //}

    private void NorthernLightsTargetSlider_eventValueChanged(UIComponent component, float value)
    {
      ccEngine.TargetNorthernLights = value;
    }

    private void NorthernLightsCurrentSlider_eventValueChanged(UIComponent component, float value)
    {
      if (this.northernLightsFixedCheckBox.isChecked)
        ccEngine.FixedNorthernLights = value;
      ccEngine.CurrentNorthernLights = value;
    }

    private void FogTargetSlider_eventValueChanged(UIComponent component, float value)
    {
      ccEngine.TargetFog = value;
    }

    private void FogCurrentSlider_eventValueChanged(UIComponent component, float value)
    {
      if (this.fogFixedCheckBox.isChecked)
        ccEngine.FixedFog = value;
      ccEngine.CurrentFog = value;
    }

    private void CloudTargetSlider_eventValueChanged(UIComponent component, float value)
    {
      ccEngine.TargetCloud = value;
    }

    private void CloudCurrentSlider_eventValueChanged(UIComponent component, float value)
    {
      if (this.cloudFixedCheckBox.isChecked)
        ccEngine.FixedCloud = value;
      ccEngine.CurrentCloud = value;
    }

    public new void Update()
    {
      if (this.IsUpdateAllowed)
      {
        this.dateTimeLabel.text = "Date time: " /*+ DayNightProperties.instance.m_TimeOfDay + " "*/ + ccEngine.GetClimateDateTime().ToString("yyyy-MM-dd HH:mm:ss (ddd)");

        int sunHour = (int)ccEngine.CurrentSolarTimeHour;
        int sunMinute = (int)((ccEngine.CurrentSolarTimeHour - sunHour) * 60);

        this.dayTimeLabel.text = "Solar time: " + string.Format("{0,2:00}:{1,2:00}", sunHour, sunMinute);

        this.dayTimeSlider.value = ccEngine.CurrentSolarTimeHour;

        this.temperatureCurrentLabel.text = "Current Temperature " + ccEngine.GetLocalizedTemperature(ccEngine.CurrentTemperature) + " (" + (ccEngine.CurrentTemperature < ccEngine.TargetTemperature ? "+" : "-") + ccEngine.GetLocalizedTemperature(ccEngine.TemperatureSpeed, 3) + ")";
        this.temperatreFixedCheckBox.isChecked = ccEngine.FixedTemperature.HasValue;
        this.temperatureCurrentSlider.value = ccEngine.CurrentTemperature;

        this.temperatureTargetLabel.text = "Target Temperature " + ccEngine.GetLocalizedTemperature(ccEngine.TargetTemperature) + " (☼" + ccEngine.CurrentWeatherProperties.m_minTemperatureDay.ToString("0.00") + " - " + ccEngine.CurrentWeatherProperties.m_maxTemperatureDay.ToString("0.00") + " / ☽" + ccEngine.CurrentWeatherProperties.m_minTemperatureNight.ToString("0.00") + " - " + ccEngine.CurrentWeatherProperties.m_maxTemperatureNight.ToString("0.00") + ")";
        this.temperatureTargetSlider.value = ccEngine.TargetTemperature;

        this.rainCurrentLabel.text = "Current Rainfall " + ccEngine.CurrentRain.ToString("0.00");
        this.rainFixedCheckBox.isChecked = ccEngine.FixedRain.HasValue;
        this.rainCurrentSlider.value = ccEngine.CurrentRain;

        this.rainTargetLabel.text = "Target Rainfall " + ccEngine.TargetRain.ToString("0.00") + " (☼" + ccEngine.DayRainProbability.ToString() + "% / ☽" + ccEngine.NightRainProbability.ToString() + "%)";
        this.rainTargetSlider.value = ccEngine.TargetRain;

        this.fogCurrentLabel.text = "Current Fog " + ccEngine.CurrentFog.ToString("0.00");
        this.fogFixedCheckBox.isChecked = ccEngine.FixedFog.HasValue;
        this.fogCurrentSlider.value = ccEngine.CurrentFog;

        this.fogTargetLabel.text = "Target Fog " + ccEngine.TargetFog.ToString("0.00") + " (☼" + ccEngine.CurrentWeatherProperties.m_fogProbabilityDay.ToString() + "% / ☽" + ccEngine.CurrentWeatherProperties.m_fogProbabilityNight.ToString() + "%)";
        this.fogTargetSlider.value = ccEngine.TargetFog;

        this.cloudCurrentLabel.text = "Current Cloud " + ccEngine.CurrentCloud.ToString("0.00");
        this.cloudFixedCheckBox.isChecked = ccEngine.FixedCloud.HasValue;
        this.cloudCurrentSlider.value = ccEngine.CurrentCloud;

        this.cloudTargetLabel.text = "Target Cloud " + ccEngine.TargetCloud.ToString("0.00") + " (☼" + ccEngine.CurrentWeatherProperties.m_cloudProbabilityDay.ToString() + "% / ☽" + ccEngine.CurrentWeatherProperties.m_cloudProbabilityNight.ToString() + "%)";
        this.cloudTargetSlider.value = ccEngine.TargetCloud;

        //this.rainbowCurrentLabel.text = "Current Rainbow " + ccEngine.CurrentRainbow.ToString("0.00");
        //this.rainbowFixedCheckBox.isChecked = ccEngine.FixedRainbow.HasValue;
        //this.rainbowCurrentSlider.value = ccEngine.CurrentRainbow;

        //this.rainbowTargetLabel.text = "Target Rainbow " + ccEngine.TargetRainbow.ToString("0.00") + " (" + ccEngine.CurrentWeatherProperties.m_rainbowProbability.ToString() + "%)";
        //this.rainbowTargetSlider.value = ccEngine.TargetRainbow;

        this.northernLightsCurrentLabel.text = "Current Northern Lights " + ccEngine.CurrentNorthernLights.ToString("0.00");
        this.northernLightsFixedCheckBox.isChecked = ccEngine.FixedNorthernLights.HasValue;
        this.northernLightsCurrentSlider.value = ccEngine.CurrentNorthernLights;

        this.northernLightsTargetLabel.text = "Target Northern Lights " + ccEngine.TargetNorthernLights.ToString("0.00") + " (" + ccEngine.CurrentWeatherProperties.m_northernLightsProbability.ToString() + "%)";
        this.northernLightsTargetSlider.value = ccEngine.TargetNorthernLights;

        this.groundWetnessLabel.text = "Ground Wetness " + ccEngine.CurrentGroundWetness.ToString("0.00");

        this.windDirectionLabel.text = "Wind Direction " + ccEngine.CurrentWindDirection.ToString("0.0") + " -> " + MathBetter.AngleSignedDelta(0f, ccEngine.WeatherManager.m_targetDirection).ToString("0.0") + " (" + ccEngine.SpeedWindDirection.ToString("0.00") + ")";
        this.windDirectionFixedCheckBox.isChecked = ccEngine.FixedWindDirection.HasValue;
        this.windDirectionSlider.value = ccEngine.CurrentWindDirection;

        this.seaLevelLabel.text = "Sea level " + ccEngine.SeaLevel.ToString("0.00");
        this.seaLevelSlider.value = ccEngine.SeaLevel;
      }
      base.Update();
    }
  }
}