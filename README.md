﻿# Cities: Skylines - Climate Control Readme

It is master climate control mod for all your climate alternation needs.

Tired of never ending summer or winter?
Want to see how temperature changes as time pass?
Want to fight wet floods and hot droughts?
Let try this mod then!

**It does not alter themes... so you will get -50C green grass on Boreal map and +40C with snow covered forests on Winter maps**

In mod options you can choose one of predefined climate settings:
North Pole, South Pole
Planet Mars (try if you want experience extremes of day/night temperature change)
London, Seville, Stockholm, Helsinki, Kraków, Cairo, Pyongyang, Moscow, Beijing, São Paulo, New York and more to come

There are two "special" presets which does not alter climate by themselves:[list]
[*][b]"Default climate settings"[/b] - will use default weather settings for currently loaded map theme. This may fix your broken weather properties but do not expect weather to behave differently than in vanilla with this mode.
[*][b]"Companion mod climate settings"[/b] - this allow using third party mods to initialize Climate control with its own climate settings. [b]If no third party mod initialize CC then this mod will not start at all![/b]
[/list]

## Other features
[list]
    [*] Choose what datatime should be taken for CC calculations
    [*] Chirp weather forecast
    [*] Snow Dumps [i](any SnowDumpAI building)[/i] melts snow faster when hot
    [*] Climate Control panel which allow for immediate changes to current weather settings
[*] Rainfall creates water - when ground is soaked rainfall may create water ponds across map
[*] Precipitation alter water sources - water target level of sources on map will alter in response to current precipitation
[/list] 

Mod can be turned on/off at will as it does not inject stuff into save game.

## Plans for future
[list]
[*] 1.2.8 [list]
[*] Fixes for new weather simulation
[*] Water sources alteration overhaul (map-bound presets introduction)
[*] New USA and Japan presets
[/list]
[*] 1.2.9 [list]
[*] Fixes for new weather simulation
[*] Events-driven climate
[*] Fix lightnings frequency
[*] New European presets
[/list]
[*] 1.3.0 - Immersive update [list]
[*] Presets management overhaul (custom presets introduction, drop of current default and companion presets support)
[*] New default climate preset produce four distinctive seasons.
[*] Better forecasts (Immersive forecasts)
[*] Introduction of weather display panel (WDP) which will become turnable from options menu (checkbox option)
[*] Current Climate control panel (CCP) will become turnable from options menu (checkbox option)
[/list]
[*] Other plans[list]
[*] Make new screenshots and some video for this mod page
[*] More climate presets (Tell me which cities/region you want!)
[*] More map presets (Tell me which maps you want!)
[*] Terrain overlay map for wind speed
[*] Find a way to replace environmental textures!
[/list]
[/list]

## DLC compatibility
Do not need any DLC (Snowfall, After Dark, Natural Disasters, Mass Transit) although some options may not work as intended.

## MOD compatibility
* [Rush Hour](http://steamcommunity.com/sharedfiles/filedetails/?id=605590542)

  Supports time used by Rush Hour mod. In order to get it working you need to select "Rush hour" in combo in this mod options menu.

* Time Warp
  
  Enabling "Alter day length" option can cause issues with proper city lighting (sun cycle related lightning).

  *As far as I can see this cannot be fixed due the way Time Warp mod assign current time (dayTimeOffsetFrames) into SimulationManager.*

* Ultimate Eyecandy 
  
  As its "time of day" related code is inherited from Time Warp having this mod enabled (even with time of day change option disabled) will cause issues related to city lighting (presets sun position, manual solar time change).

## Recommended mods
* [Winter Buildings Unlocker (+vice versa)](http://steamcommunity.com/sharedfiles/filedetails/?id=627047745)
Allows you to make winter-themed buildings which are really usefull if you get snowed roads on your paradaise island.

## Map compatibility
In general all CC functionalities are available for all maps, however some more advanced concepts (like water sources alternation) can be heavily tweaked and customized for each map design quirks. To do this tweaking however I need to know which map should be tweaked (workshop is too big to tweak them all) and therefore I welcome everyone to post their requests regarding map presets [url=http://steamcommunity.com/workshop/filedetails/discussion/629713122/1470840994963619117/]here[/url].

## Source code & issue tracker
[Bitbucket repository](https://bitbucket.org/Runaurufu/cities-skylines-climate-control/overview)

If you have experienced problem and want to share with output_log/callstack/whatever - please insert ticket into [issue tracker](https://bitbucket.org/Runaurufu/cities-skylines-climate-control/issues?status=new&status=open).

## Donate
[Please ![](https://www.paypalobjects.com/en_GB/i/btn/btn_donate_LG.gif) to make me happy :)](https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=runaurufu%40gmail.com&lc=US&item_name=Cities:&nbsp;Skylines&nbsp;Climate&nbsp;Control&no_note=0&currency_code=PLN&bn=PP-DonationsBF%3abtn_donateCC_LG.gif%3aNonHosted)
