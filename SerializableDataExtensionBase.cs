﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using ICities;
using Runaurufu.Utility;
using System.Xml.Serialization;
using System.Xml;
using ColossalFramework;

namespace Runaurufu.ClimateControl
{
  public class ClimateControlSerializableData : SerializableDataExtensionBase
  {
    private const string KEY_HISTORY_DATA = "CLIMATE_CONTROL_HISTORICAL_DATA";
    private const string KEY_WEATHER_STATE = "CLIMATE_CONTROL_WEATHER_STATE";
    private const string KEY_DEFAULT_WATER_SOURCES = "CLIMATE_CONTROL_DEFAULT_WATER_SOURCES";
    private ISerializableData serializableData;

    public override void OnCreated(ISerializableData serializableData)
    {
      //base.OnCreated(serializableData);

      this.serializableData = serializableData;
    }

    public override void OnLoadData()
    {
      //base.OnLoadData();

      try
      {
        byte[] data;

        data = serializableData.LoadData(ClimateControlSerializableData.KEY_DEFAULT_WATER_SOURCES);
        if (data != null && data.Length > 0)
        {
          DefaultWaterSourceData[] arr = Deserialize<DefaultWaterSourceData[]>(data);
          if (arr != null)
          {
            this.RestoreDefaultWaterSourcesValues(arr);            
          }
        }

        data = serializableData.LoadData(ClimateControlSerializableData.KEY_WEATHER_STATE);
        if (data != null && data.Length > 0)
        {
          WeatherState state = XmlDeserialize<WeatherState>(data);
          if (state != null)
            ClimateControlEngine.GetInstance().WeatherState = state;
        }

        data = serializableData.LoadData(ClimateControlSerializableData.KEY_HISTORY_DATA);
        if (data != null && data.Length > 0)
        {
          HistoryData history = Deserialize<HistoryData>(data);
          if (history != null)
          {
            ClimateControlEngine.GetInstance().HistoricalData = history;

            //if (history.YearlyData != null)
            //{
            //  DebugOutputPanel.AddMessage(ColossalFramework.Plugins.PluginManager.MessageType.Message, history.YearlyData.Count.ToString());
            //  foreach (YearlyStatisticData yitem in history.YearlyData)
            //  {
            //    DebugOutputPanel.AddMessage(ColossalFramework.Plugins.PluginManager.MessageType.Message, "YEAR " + yitem.Year.ToString());
            //    if (yitem.WeeklyData != null)
            //    {
            //      foreach (WeeklyStatisticData witem in yitem.WeeklyData)
            //      {
            //        if (witem == null)
            //          continue;
            //        DebugOutputPanel.AddMessage(ColossalFramework.Plugins.PluginManager.MessageType.Message, "WEEK " + witem.WeekIndex.ToString());
            //        DebugOutputPanel.AddMessage(ColossalFramework.Plugins.PluginManager.MessageType.Message, "Tav " + witem.TemperatureAverage.ToString());
            //        DebugOutputPanel.AddMessage(ColossalFramework.Plugins.PluginManager.MessageType.Message, "P " + witem.PrecipitationAmount.ToString());
            //      }
            //    }
            //  }
            //}
          }
        }
      }
      catch (Exception ex)
      {
        Logger.Log("SerializableDataExtensionBase.OnLoadData() exception!" + Environment.NewLine + ex.ToString());
      }
    }

    public override void OnSaveData()
    {
      //base.OnSaveData();

      try
      {
        //if (ClimateControlEngine.Instance.DefaultMapWaterSources != null)
        //{
        //  byte[] data = Serialize(ClimateControlEngine.Instance.DefaultMapWaterSources);
        //  if (data != null)
        //  {
        //    serializableData.SaveData(ClimateControlSerializableData.KEY_DEFAULT_WATER_SOURCES, data);
        //  }
        //}

        if (ClimateControlEngine.GetInstance().WeatherState != null)
        {
          byte[] data = XmlSerialize(ClimateControlEngine.GetInstance().WeatherState);
          if (data != null)
          {
            serializableData.SaveData(ClimateControlSerializableData.KEY_WEATHER_STATE, data);
          }
        }

        if (ClimateControlEngine.GetInstance().HistoricalData != null)
        {
          byte[] data = Serialize(ClimateControlEngine.GetInstance().HistoricalData);
          if (data != null)
            serializableData.SaveData(ClimateControlSerializableData.KEY_HISTORY_DATA, data);

          //if (ClimateControlEngine.Instance.HistoricalData.YearlyData != null)
          //{
          //  DebugOutputPanel.AddMessage(ColossalFramework.Plugins.PluginManager.MessageType.Message, ClimateControlEngine.Instance.HistoricalData.YearlyData.Count.ToString());
          //  foreach (YearlyStatisticData yitem in ClimateControlEngine.Instance.HistoricalData.YearlyData)
          //  {
          //    DebugOutputPanel.AddMessage(ColossalFramework.Plugins.PluginManager.MessageType.Message, "YEAR " + yitem.Year.ToString());
          //    if (yitem.WeeklyData != null)
          //    {
          //      foreach (WeeklyStatisticData witem in yitem.WeeklyData)
          //      {
          //        if (witem == null)
          //          continue;
          //        DebugOutputPanel.AddMessage(ColossalFramework.Plugins.PluginManager.MessageType.Message, "WEEK " + witem.WeekIndex.ToString());
          //        DebugOutputPanel.AddMessage(ColossalFramework.Plugins.PluginManager.MessageType.Message, "Tav " + witem.TemperatureAverage.ToString());
          //        DebugOutputPanel.AddMessage(ColossalFramework.Plugins.PluginManager.MessageType.Message, "P " + witem.PrecipitationAmount.ToString());
          //      }
          //    }
          //  }
          //}
        }
      }
      catch (Exception ex)
      {
        Logger.Log("SerializableDataExtensionBase.OnLoadData() exception!" + Environment.NewLine + ex.ToString());
      }
    }

    public static byte[] Serialize(object item)
    {
      BinaryFormatter binaryFormatter = new BinaryFormatter();
      using (MemoryStream memoryStream = new MemoryStream())
      {
        try
        {
          binaryFormatter.Serialize(memoryStream, item);
          memoryStream.Position = 0L;
          return memoryStream.ToArray();
        }
        catch (Exception ex)
        {
          Logger.Log("Serialize Exception:" + Environment.NewLine + ex.ToString());
        }
        return null;
      }
    }

    public static T Deserialize<T>(byte[] data) where T : class
    {
      BinaryFormatter binaryFormatter = new BinaryFormatter();
      using (MemoryStream memoryStream = new MemoryStream())
      {
        memoryStream.Write(data, 0, data.Length);
        memoryStream.Position = 0L;
        try
        {
          return binaryFormatter.Deserialize(memoryStream) as T;
        }
        catch (Exception ex)
        {
          Logger.Log("Deserialize Exception:" + Environment.NewLine + ex.ToString());
        }
        return null;
      }
    }

    public static byte[] XmlSerialize(object item)
    {
      if (item == null)
        return null;

      using (MemoryStream memoryStream = new MemoryStream())
      {
        try
        {
          XmlSerializer serializer = new XmlSerializer(item.GetType());
          using (XmlWriter xmlWriter = XmlWriter.Create(memoryStream))
          {
            serializer.Serialize(xmlWriter, item);
          }

          memoryStream.Position = 0L;
          return memoryStream.ToArray();
        }
        catch (Exception ex)
        {
          Logger.Log("XmlSerialize Exception:" + Environment.NewLine + ex.ToString());
        }
        return null;
      }
    }

    public static T XmlDeserialize<T>(byte[] data) where T : class
    {
      try
      {
        using (MemoryStream memoryStream = new MemoryStream())
        {
          memoryStream.Write(data, 0, data.Length);
          memoryStream.Position = 0L;

          XmlSerializer serializer = new XmlSerializer(typeof(T));
          using (XmlReader xmlReader = XmlReader.Create(memoryStream))
          {
            return (T)serializer.Deserialize(xmlReader);
          }
        }
      }
      catch (Exception ex)
      {
        Logger.Log("XmlDeserialize Exception:" + Environment.NewLine + ex.ToString());
        return null;
      }
    }

    private void RestoreDefaultWaterSourcesValues(DefaultWaterSourceData[] defaultSources)
    {
      FastList<WaterSource> waterSources = Singleton<TerrainManager>.instance.WaterSimulation.m_waterSources;

      for (int i = 0; i < defaultSources.Length; i++)
      {
        if (waterSources.m_buffer.Length <= defaultSources[i].Index)
          continue;

        if (waterSources.m_buffer[defaultSources[i].Index].m_type != WaterSource.TYPE_NATURAL)
          continue;

        waterSources.m_buffer[defaultSources[i].Index].m_target = defaultSources[i].Target;
        waterSources.m_buffer[defaultSources[i].Index].m_inputRate = defaultSources[i].InputRate;
        waterSources.m_buffer[defaultSources[i].Index].m_outputRate = defaultSources[i].OutputRate;
      }
    }
  }

  [Serializable]
  public class DefaultWaterSourceData
  {
    public int Index;
    public ushort Target;
    public uint InputRate;
    public uint OutputRate;
  }
}