﻿using Runaurufu.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Runaurufu.ClimateControl
{
  public class WeatherState
  {
    public WeatherValue Rainbow;
    public WeatherValue NorthernLights;
    public WeatherValue GroundWetness;
    public WeatherValue Cloud;
    public WeatherValue Fog;
    public WeatherValue Rain;
    public WeatherValue Temperature;
    public WeatherValue WindDirection;

    /// <summary>
    /// Water quantity which is stored in air over the map. [waterCellHeight]
    /// </summary>
    public float AirWater;
    /// <summary>
    /// Water quantity which is stored in clouds over the map. [waterCellHeight]
    /// </summary>
    public float CloudsWater;
    /// <summary>
    /// On land water quantity which did not turned into actual water stored in cells. [waterCellHeight]
    /// </summary>
    public float SurfaceWater;
    /// <summary>
    /// It is temporary hold water in soil itself. It is accumulated from surface water and slowly transit into ground water. [waterCellHeight]
    /// </summary>
    public float SoilWater;
    /// <summary>
    /// Water quantity which is stored in soil on map. [waterCellHeight]
    /// </summary>
    public float GroundWater;
    /// <summary>
    /// Ground and surface water quantity which is available outside of map. [waterCellHeight]
    /// </summary>
    public float OutOfMapGroundWater;
    /// <summary>
    /// Clouds and air water quantity which is available outside of map. [waterCellHeight]
    /// </summary>
    public float OutOfMapAirWater;

    // TODO Yup they should affect available sun-rays :o
    //public WeatherValue LowLevelAirDust;
    //public WeatherValue MediumLevelAirDust;
    //public WeatherValue HighLevelAirDust;
    // also what about taking care of actual size of air pollution particles?


    public float GetAirPressure()
    {
      /// temp, water in air... something must come from this


      return 0;
    }



    public void SetToWeatherManager()
    {
      ClimateControlEngine cce = ClimateControlEngine.GetInstance();
      cce.WeatherManager.m_currentRainbow = this.Rainbow.Value;
      cce.WeatherManager.m_targetRainbow = this.Rainbow.Target;
      cce.WeatherManager.m_currentNorthernLights = this.NorthernLights.Value;
      cce.WeatherManager.m_targetNorthernLights = this.NorthernLights.Target;
      cce.WeatherManager.m_groundWetness = this.GroundWetness.Value;
      cce.WeatherManager.m_currentCloud = this.Cloud.Value;
      cce.WeatherManager.m_targetCloud = this.Cloud.Target;
      cce.WeatherManager.m_currentFog = this.Fog.Value;
      cce.WeatherManager.m_targetFog = this.Fog.Target;
      cce.WeatherManager.m_currentRain = this.Rain.Value;
      cce.WeatherManager.m_targetRain = this.Rain.Target;
      cce.WeatherManager.m_currentTemperature = this.Temperature.Value;
      cce.WeatherManager.m_targetTemperature = this.Temperature.Target;
      cce.WeatherManager.m_temperatureSpeed = this.Temperature.Speed;
      cce.WeatherManager.m_windDirection = this.WindDirection.Value;
      cce.WeatherManager.m_targetDirection = this.WindDirection.Target;
      cce.WeatherManager.m_directionSpeed = this.WindDirection.Speed;
    }

    public void SetFromWeatherManager()
    {
      ClimateControlEngine cce = ClimateControlEngine.GetInstance();

      this.Rainbow.Value = cce.WeatherManager.m_currentRainbow;
      this.Rainbow.Target = cce.WeatherManager.m_targetRainbow;
      this.NorthernLights.Value = cce.WeatherManager.m_currentNorthernLights;
      this.NorthernLights.Target = cce.WeatherManager.m_targetNorthernLights;
      this.GroundWetness.Value = cce.WeatherManager.m_groundWetness;
      this.Cloud.Value = cce.WeatherManager.m_currentCloud;
      this.Cloud.Target = cce.WeatherManager.m_targetCloud;
      this.Fog.Value = cce.WeatherManager.m_currentFog;
      this.Fog.Target = cce.WeatherManager.m_targetFog;
      this.Rain.Value = cce.WeatherManager.m_currentRain;
      this.Rain.Target = cce.WeatherManager.m_targetRain;
      this.Temperature.Value = cce.WeatherManager.m_currentTemperature;
      this.Temperature.Target = cce.WeatherManager.m_targetTemperature;
      this.Temperature.Speed = cce.WeatherManager.m_temperatureSpeed;
      this.WindDirection.Value = cce.WeatherManager.m_windDirection;
      this.WindDirection.Target = cce.WeatherManager.m_targetDirection;
      this.WindDirection.Speed = cce.WeatherManager.m_directionSpeed;
    }

    public void UpdateState(TimeSpan delta)
    {
      ClimateControlEngine cce = ClimateControlEngine.GetInstance();

      this.Rainbow.UpdateState(delta, RainbowMinValue, RainbowMaxValue, cce.FixedRainbow);
      this.NorthernLights.UpdateState(delta, NorthernLightsMinValue, NorthernLightsMaxValue, cce.FixedNorthernLights);
      this.GroundWetness.UpdateState(delta, 0f, 1f, cce.FixedGroundWetness);
      this.Cloud.UpdateState(delta, 0f, 1f, cce.FixedCloud);
      this.Fog.UpdateState(delta, 0f, 1f, cce.FixedFog);
      this.Rain.UpdateState(delta, RainMinValue, RainMaxValue, cce.FixedRain);
      this.Temperature.UpdateState(delta, -273.15f, 30000.0f, cce.FixedTemperature);
      this.WindDirection.UpdateAngleState(delta, -180f, 180f, cce.FixedWindDirection);
    }

    internal const float RainMinValue = 0f; // vanilla: 0f
    internal const float RainMaxValue = 4f; // vanilla: 1f

    internal const float RainbowMinValue = 0f; // vanilla: 0f
    internal const float RainbowMaxValue = 1f; // vanilla: 1f

    internal const float NorthernLightsMinValue = 0f; // vanilla: 0f
    internal const float NorthernLightsMaxValue = 100f; // vanilla: 1f

  }

  public struct WeatherValue
  {
    public float Value;
    public float Target;
    public float Speed;

    public void UpdateState(TimeSpan timeDelta, float min, float max, float? fixedValue)
    {
      if (fixedValue.HasValue)
      {
        this.Value = fixedValue.Value;
        this.Target = fixedValue.Value;
        this.Speed = 0.0f;
        return;
      }

      if (this.Speed == 0.0f)
        return;

      float progress = this.Speed * (float)timeDelta.TotalMinutes;

      float newValue = this.Value + progress;

      if (this.Value < this.Target && newValue < this.Target
        || this.Value > this.Target && newValue > this.Target)
      {
        // pretty much is ok
        this.Value = newValue;
      }
      else if (this.Speed < 0.001f)
      {
        this.Value = this.Target;
        this.Speed = 0.0f;
      }
      else
      {
        this.Value = newValue;
        this.Speed *= 0.5f;
      }


      if (this.Value >= max)
      {
        this.Value = max;
        if (this.Speed > 0.0f)
          this.Speed = 0.0f;
      }
      else if (this.Value < min)
      {
        this.Value = min;
        if (this.Speed < 0.0f)
          this.Speed = 0.0f;
      }
    }

    public void ApplySpeedAcceleration(float speedAcceleration, float directionChangeSpeedAcceleration)
    {
      float delta = this.Target - this.Value;
      if (Math.Sign(delta) == Math.Sign(this.Speed))
        this.Speed = Mathf.MoveTowards(this.Speed, delta, speedAcceleration);
      else
        this.Speed = Mathf.MoveTowards(this.Speed, delta, directionChangeSpeedAcceleration);
    }

    public void UpdateAngleState(TimeSpan timeDelta, float min, float max, float? fixedValue)
    {
      if (fixedValue.HasValue)
      {
        this.Value = MathBetter.AngleSignedDelta(0f, fixedValue.Value);
        return;
      }

      float progress = this.Speed * (float)timeDelta.TotalMinutes;

      // 0 - 360
      //this.Target = Mathf.Repeat(this.Target, 360f);

      // -180 - 180
      this.Value = MathBetter.AngleSignedDelta(0f, this.Value + progress);
    }

    public void ApplyAngleSpeedAcceleration(float speedAcceleration, float directionChangeSpeedAcceleration)
    {
      float delta = MathBetter.AngleSignedDelta(this.Value, this.Target) * 0.001f;

      if (Math.Sign(delta) == Math.Sign(this.Speed))
        this.Speed = Mathf.MoveTowards(this.Speed, delta, speedAcceleration);
      else
        this.Speed = Mathf.MoveTowards(this.Speed, delta, directionChangeSpeedAcceleration);
    }
  }
}
