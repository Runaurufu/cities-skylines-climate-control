﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Runaurufu.ClimateControl
{
  [Serializable]
  public class ClimateControlProperties
  {
    /// <summary>
    /// If temperature falls below that value you will get snow instead of rain.
    /// </summary>
    public float SnowFallTemperature { get; set; }

    /// <summary>
    /// If temperature falls below that value all your snow should melt away.
    /// </summary>
    public float SnowMeltTemperature { get; set; }

    /// <summary>
    /// How many earth hours day takes? For Earth = 24.0f!
    /// </summary>
    public float SolarDayLength { get; set; }

    /// <summary>
    /// Longitude, range: -180, 180
    /// </summary>
    public float Longitude { get; set; }

    /// <summary>
    /// Latitude, range: -80, 80
    /// </summary>
    public float Latitude { get; set; }

    /// <summary>
    /// Climate date for months to come. Set this to null to use only weather properties for temperature calculation.
    /// </summary>
    public MonthlyClimateData[] ClimateData { get; set; }

    internal static ClimateControlProperties GetDefaults()
    {
      ClimateControlProperties properties = new ClimateControlProperties();
      properties.SnowFallTemperature = 2;
      properties.SnowMeltTemperature = 10;
      properties.SolarDayLength = 24.0f;
      properties.Longitude = float.NaN;
      properties.Latitude = float.NaN;
      properties.ClimateData = null;

      return properties;
    }
  }
}
