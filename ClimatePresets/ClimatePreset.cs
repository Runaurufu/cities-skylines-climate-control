﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Runaurufu.ClimateControl
{
  [Serializable]
  public class ClimatePreset
  {
    public String PresetName { get; set; }

    /// <summary>
    /// It must be unique climate code!
    /// </summary>
    public String PresetCode { get; set; }

    public ClimateControlProperties ClimateProperties { get; set; }
  }
}
