﻿using ColossalFramework;
using ICities;
using Runaurufu.Common;
using Runaurufu.Utility;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

namespace Runaurufu.ClimateControl
{
  public class ClimateControlEngine : IClimateControlEngine
  {
    private static ClimateControlEngine instance = null;

    public static ClimateControlEngine GetInstance()
    {
      if (instance == null)
        instance = new ClimateControlEngine();
      return instance;

    }

    private ClimateControlEngine()
    {
      this.IsInitialized = false;
      this.random = new System.Random((int)DateTime.Now.TimeOfDay.TotalMinutes);
      this.InitializeManagers();
    }

    public HistoryData HistoricalData { get; set; }

    internal WeatherManager WeatherManager { get; set; }
    private TerrainManager terrainManager;
    private SimulationManager simulationManager;
    private NetManager netManager;
    private BuildingManager buildingManager;
    internal IThreading ThreadingManager { get; set; }

    /// <summary>
    /// RushHours mod date time.
    /// </summary>
    private FieldInfo rushHoursDateTime = null;

    public bool IsInitialized { get; private set; }

    public WeatherProperties CurrentWeatherProperties
    {
      get { return this.WeatherManager.m_properties; }
      private set
      {
        this.WeatherManager.InitializeProperties(value);
      }
    }

    /// <summary>
    /// Sets WeatherManager targets to match current values.
    /// </summary>
    public void SetWeatherToEquilibrium()
    {
      this.WeatherManager.m_targetCloud = this.WeatherManager.m_currentCloud;
      this.WeatherManager.m_targetFog = this.WeatherManager.m_currentFog;
      this.WeatherManager.m_targetNorthernLights = this.WeatherManager.m_currentNorthernLights;
      this.WeatherManager.m_targetRain = this.WeatherManager.m_currentRain;
      this.WeatherManager.m_targetRainbow = this.WeatherManager.m_currentRainbow;
      this.WeatherManager.m_targetTemperature = this.WeatherManager.m_currentTemperature;
      this.WeatherManager.m_temperatureSpeed = 0;
      this.WeatherManager.m_targetDirection = MathBetter.Mod(this.WeatherManager.m_windDirection, 360f);
      this.WeatherManager.m_directionSpeed = 0;
    }

    public ClimateControlProperties ClimateControlProperties { get; private set; }

    public ClimateFrameData[] ClimateFrames { get; private set; }
    public ClimateGlobalValues ClimateGlobals { get; private set; }

    public DefaultSettings DefaultSettings { get; private set; }

    private System.Random random = null;

    public bool Initialize(WeatherProperties weatherProperties)
    {
      if (weatherProperties == null)
        return false;

      if (this.InitializeManagers() == false)
        return false;

      if (this.InitializeConfigValues() == false)
        return false;

      this.CurrentWeatherProperties = weatherProperties;
      this.ClimateControlProperties = ClimateControlProperties.GetDefaults();

      this.ResetInternalValues();
      //this.CreateDefaultMapWaterSources();
      this.InitializeClimateFrames();

      this.WeatherManager.m_enableWeather = true;
      this.WeatherManager.m_forceWeatherOn += 1.0f;

      this.IsInitialized = true;

      return true;
    }

    public bool Initialize(ClimateControlProperties climateProperties)
    {
      if (climateProperties == null)
        return false;

      if (this.InitializeManagers() == false)
        return false;

      if (this.InitializeConfigValues() == false)
        return false;

      if (this.InitializeCrossModSupport() == false)
        return false;

      this.ClimateControlProperties = climateProperties;

      this.ResetInternalValues();
      //this.CreateDefaultMapWaterSources();
      this.InitializeClimateFrames();

      // should we enable or disable this?
      // if (WeatherManager.m_enableWeather == false && WeatherManager.m_forceWeatherOn < 1.0f)
      this.WeatherManager.m_enableWeather = true;
      this.WeatherManager.m_forceWeatherOn += 1.0f;

      this.IsInitialized = true;

      return true;
    }

    private bool InitializeManagers()
    {
      TerrainManager tm = Singleton<TerrainManager>.instance;
      if (tm == null)
        return false;

      WeatherManager wm = Singleton<WeatherManager>.instance;
      if (wm == null)
        return false;

      SimulationManager sm = Singleton<SimulationManager>.instance;
      if (sm == null)
        return false;

      NetManager nm = Singleton<NetManager>.instance;
      if (nm == null)
        return false;

      BuildingManager bm = Singleton<BuildingManager>.instance;
      if (bm == null)
        return false;

      this.terrainManager = tm;
      this.WeatherManager = wm;
      this.simulationManager = sm;
      this.netManager = nm;
      this.buildingManager = bm;
      return true;
    }

    private bool InitializeCrossModSupport()
    {
      // Rush Hours support
      Type rhManager = Type.GetType("RushHour.Events.CityEventManager");
      if (rhManager != null)
      {
        FieldInfo fieldInfo = rhManager.GetField("CITY_TIME", System.Reflection.BindingFlags.Static | BindingFlags.Public);
        if (fieldInfo != null && fieldInfo.GetValue(null) is DateTime)
        {
          this.rushHoursDateTime = fieldInfo;
        }
      }
      return true;
    }

    internal bool InitializeConfigValues()
    {
      switch (GlobalConfig.GetInstance().ThundersFrequency)
      {
        case Frequency.AlmostNever:
          this.ThundersFrequency = new Vector2(100f, 0f);
          break;

        case Frequency.Rarely:
          this.ThundersFrequency = new Vector2(32f, 1f);
          break;

        case Frequency.BelowAverage:
          this.ThundersFrequency = new Vector2(16f, 4f);
          break;

        case Frequency.AboveAverage:
          this.ThundersFrequency = new Vector2(4f, 8f);
          break;

        case Frequency.Often:
          this.ThundersFrequency = new Vector2(1f, 16f);
          break;

        case Frequency.AlmostConstantly:
          this.ThundersFrequency = new Vector2(0f, 100f);
          break;

        case Frequency.OnAverage:
        default:
          this.ThundersFrequency = new Vector2(5f, 5f);
          break;
      }

      return true;
    }

    public void InitializeClimateFrames()
    {
      if (this.ClimateControlProperties == null || this.ClimateControlProperties.ClimateData == null || this.ClimateControlProperties.ClimateData.Length == 0)
      {
        this.ClimateFrames = null;
        this.ClimateGlobals = null;
        return;
      }

      if (float.IsNaN(this.ClimateControlProperties.Latitude) == false)
        DayNightProperties.instance.m_Latitude = this.ClimateControlProperties.Latitude;
      if (float.IsNaN(this.ClimateControlProperties.Longitude) == false)
        DayNightProperties.instance.m_Longitude = this.ClimateControlProperties.Longitude;

      float daysPerClimateStage = 366f / this.ClimateControlProperties.ClimateData.Length;

      float smoothingDays = 0;

      if (daysPerClimateStage > 3 && this.ClimateControlProperties.ClimateData.Length > 2 && GlobalConfig.GetInstance().ClimateSmoothness != Level.None) // to keep smoothing in reasonable boundaries
      {
        smoothingDays = daysPerClimateStage * (int)GlobalConfig.GetInstance().ClimateSmoothness
          / (2f /* This should provide half of smooth length per frame*/
          * 2f /* Smooth should not be longer then normal frame*/
          * 8f /* Level gives 0-8 range */);
      }

      float climateFrameDayLength = daysPerClimateStage - 2 * smoothingDays;
      float normalFrameProgress = climateFrameDayLength / 366f;

      if (smoothingDays != 0)
      {
        // generate with smoothing!
        float smoothFrameProgress = smoothingDays / 366f;

        this.ClimateFrames = new ClimateFrameData[this.ClimateControlProperties.ClimateData.Length * 3];

        ClimateFrameData aux1, aux2;

        ClimateFrameData.CreateSmoothingFrames(this.ClimateControlProperties.ClimateData[this.ClimateControlProperties.ClimateData.Length - 1], this.ClimateControlProperties.ClimateData[0], this.ClimateControlProperties.SolarDayLength, out aux1, out aux2);

        aux1.YearProgressEnd = 1;
        aux1.YearProgressStart = 1 - smoothFrameProgress;
        aux2.YearProgressStart = 0;
        aux2.YearProgressEnd = smoothFrameProgress;

        this.ClimateFrames[0] = aux2;
        this.ClimateFrames[this.ClimateFrames.Length - 2] = new ClimateFrameData(this.ClimateControlProperties.ClimateData[this.ClimateControlProperties.ClimateData.Length - 1], aux1.YearProgressStart - normalFrameProgress, aux1.YearProgressStart, this.ClimateControlProperties.SolarDayLength);
        this.ClimateFrames[this.ClimateFrames.Length - 1] = aux1;

        for (int i = 0; i < this.ClimateControlProperties.ClimateData.Length - 1; i++)
        {
          ClimateFrameData.CreateSmoothingFrames(this.ClimateControlProperties.ClimateData[i], this.ClimateControlProperties.ClimateData[i + 1], this.ClimateControlProperties.SolarDayLength, out aux1, out aux2);

          float start = this.ClimateFrames[i * 3].YearProgressEnd;

          ClimateFrameData frame = new ClimateFrameData(this.ClimateControlProperties.ClimateData[i], start, start + normalFrameProgress, this.ClimateControlProperties.SolarDayLength);
          this.ClimateFrames[i * 3 + 1] = frame;
          aux1.YearProgressStart = frame.YearProgressEnd;
          aux1.YearProgressEnd = aux1.YearProgressStart + smoothFrameProgress;
          this.ClimateFrames[i * 3 + 2] = aux1;
          aux2.YearProgressStart = aux1.YearProgressEnd;
          aux2.YearProgressEnd = aux2.YearProgressStart + smoothFrameProgress;
          this.ClimateFrames[i * 3 + 3] = aux2;
        }
      }
      else
      {
        this.ClimateFrames = new ClimateFrameData[this.ClimateControlProperties.ClimateData.Length];

        for (int i = 0; i < this.ClimateControlProperties.ClimateData.Length; i++)
        {
          MonthlyClimateData data = this.ClimateControlProperties.ClimateData[i];

          ClimateFrameData frame = new ClimateFrameData(data, i * normalFrameProgress, (i + 1) * normalFrameProgress, this.ClimateControlProperties.SolarDayLength);
          this.ClimateFrames[i] = frame;
        }
      }

      this.ClimateGlobals = ClimateGlobalValues.Create(this.ClimateFrames);

      // ensure that we will immediatelly apply changes!
      this.currentClimateFrameData = null;
    }

    public void Uninitialize()
    {
      //MapThemeMetaData mapThemeMetaData = this.simulationManager.m_metaData.m_MapThemeMetaData;
      this.WeatherManager.InitializeProperties(GetDefaultWeatherProperites());

      // Reset SimulationManager!
      SimulationManager.SUNRISE_HOUR = 5f;
      SimulationManager.SUNSET_HOUR = 20f;
      SimulationManager.RELATIVE_DAY_LENGTH = 5;
      SimulationManager.RELATIVE_NIGHT_LENGTH = 3;

      this.ResetInternalValues();

      this.HistoricalData = null;
      this.IsInitialized = false;
    }

    private void ResetInternalValues()
    {
      this.lastClimateSimulationTimeUpdate = DateTime.MinValue;

      this.weatherState = null;

      this.ClimateFrames = null;
      this.ClimateGlobals = null;
    }

    public static WeatherProperties GetDefaultWeatherProperites()
    {
      WeatherProperties weatherProperties = new WeatherProperties();
      weatherProperties.InvokeMethod("OverrideFromMapTheme");
      return weatherProperties;
    }

    #region Temperature Get&Set
    public float CurrentTemperature
    {
      get
      {
        if (this.weatherState != null)
          return this.weatherState.Temperature.Value;
        else
          return this.WeatherManager.m_currentTemperature;
      }

      set
      {
        this.WeatherManager.m_currentTemperature = value;
        if (this.weatherState != null)
          this.weatherState.Temperature.Value = value;
      }
    }

    public float TargetTemperature
    {
      get
      {
        if (this.weatherState != null)
          return this.weatherState.Temperature.Target;
        else
          return this.WeatherManager.m_targetTemperature;
      }

      set
      {
        bool reinitializeProperties = true;

        if (this.IsNight(this.GetClimateDateTime()))
        {
          if (value < this.WeatherManager.m_properties.m_minTemperatureNight)
            this.WeatherManager.m_properties.m_minTemperatureNight = value - 1.05f;
          else if (value > this.WeatherManager.m_properties.m_maxTemperatureNight)
            this.WeatherManager.m_properties.m_maxTemperatureNight = value + 1.05f;
          else
            reinitializeProperties = false;
        }
        else
        {
          if (value < this.WeatherManager.m_properties.m_minTemperatureDay)
            this.WeatherManager.m_properties.m_minTemperatureDay = value - 1.05f;
          else if (value > this.WeatherManager.m_properties.m_maxTemperatureDay)
            this.WeatherManager.m_properties.m_maxTemperatureDay = value + 1.05f;
          else
            reinitializeProperties = false;
        }

        if (reinitializeProperties)
        {
          // Set min-maxes for fog and rain to avoid instant target recalculation
          this.WeatherManager.m_properties.m_minTemperatureRain = Mathf.Min(this.WeatherManager.m_properties.m_minTemperatureDay, this.WeatherManager.m_properties.m_minTemperatureNight);
          this.WeatherManager.m_properties.m_maxTemperatureRain = Mathf.Max(this.WeatherManager.m_properties.m_maxTemperatureDay, this.WeatherManager.m_properties.m_maxTemperatureNight);

          this.WeatherManager.m_properties.m_minTemperatureFog = this.WeatherManager.m_properties.m_minTemperatureRain;
          this.WeatherManager.m_properties.m_maxTemperatureFog = this.WeatherManager.m_properties.m_maxTemperatureRain;

          this.WeatherManager.InitializeProperties(this.WeatherManager.m_properties);
        }

        this.WeatherManager.m_targetTemperature = value;
        if (this.weatherState != null)
          this.weatherState.Temperature.Target = value;
      }
    }

    public float TemperatureSpeed
    {
      get
      {
        if (this.weatherState != null)
          return this.weatherState.Temperature.Speed;
        else
          return this.WeatherManager.m_temperatureSpeed;
      }
      internal set
      {
        this.WeatherManager.m_temperatureSpeed = value;
        if (this.weatherState != null)
          this.weatherState.Temperature.Speed = value;
      }
    }
    #endregion

    #region Rain Get&Set

    public float CurrentRain
    {
      get
      {
        if (this.weatherState != null)
          return this.weatherState.Rain.Value;
        else
          return this.WeatherManager.m_currentRain;
      }
      set
      {
        this.WeatherManager.m_currentRain = value;
        if (this.weatherState != null)
          this.weatherState.Rain.Value = value;
      }
    }

    public float TargetRain
    {
      get
      {
        if (this.weatherState != null)
          return this.weatherState.Rain.Target;
        else
          return this.WeatherManager.m_targetRain;
      }
      set
      {
        this.WeatherManager.m_targetRain = value;
        if (this.weatherState != null)
          this.weatherState.Rain.Target = value;
      }
    }
    #endregion

    public int DayRainProbability
    {
      get { return this.CurrentWeatherProperties.m_rainProbabilityDay; }
      internal set { this.CurrentWeatherProperties.m_rainProbabilityDay = value; }
    }

    public int NightRainProbability
    {
      get { return this.CurrentWeatherProperties.m_rainProbabilityNight; }
      internal set { this.CurrentWeatherProperties.m_rainProbabilityNight = value; }
    }

    public float MaxRainIntensity
    {
      get
      {
        if (DayNightProperties.instance != null)
        {
          RainParticleProperties rainParticleProperties = DayNightProperties.instance.GetComponent<RainParticleProperties>();
          if (rainParticleProperties != null)
          {
            return rainParticleProperties.m_MaxRainIntensity;
          }
        }
        return float.NaN;
      }

      internal set
      {
        if (DayNightProperties.instance != null)
        {
          RainParticleProperties rainParticleProperties = DayNightProperties.instance.GetComponent<RainParticleProperties>();
          if (rainParticleProperties != null)
          {
            rainParticleProperties.m_MaxRainIntensity = value;
          }
        }
      }
    }

    public float LightningRainTreshold
    {
      get
      {
        if (DayNightProperties.instance != null)
        {
          RainParticleProperties rainParticleProperties = DayNightProperties.instance.GetComponent<RainParticleProperties>();
          if (rainParticleProperties != null)
          {
            //return rainParticleProperties.m_LightningTreshold;
          }
        }
        return float.NaN;
      }

      internal set
      {
        if (DayNightProperties.instance != null)
        {
          RainParticleProperties rainParticleProperties = DayNightProperties.instance.GetComponent<RainParticleProperties>();
          if (rainParticleProperties != null)
          {
            //rainParticleProperties.m_LightningTreshold = value;
          }
        }
      }
    }

    private Vector2 ThundersFrequency;

    #region FixedWeatherValues

    public float? FixedRainbow;
    public float? FixedNorthernLights;
    public float? FixedGroundWetness;
    public float? FixedCloud;
    public float? FixedFog;
    public float? FixedRain;
    public float? FixedTemperature;
    public float? FixedWindDirection;

    #endregion FixedWeatherValues

    public float CurrentFog
    {
      get
      {
        if (this.weatherState != null)
          return this.weatherState.Fog.Value;
        else
          return this.WeatherManager.m_currentFog;
      }
      internal set
      {
        this.WeatherManager.m_currentFog = value;
        if (this.weatherState != null)
          this.weatherState.Fog.Value = value;
      }
    }

    public float TargetFog
    {
      get
      {
        if (this.weatherState != null)
          return this.weatherState.Fog.Target;
        else
          return this.WeatherManager.m_targetFog;
      }
      internal set
      {
        this.WeatherManager.m_targetFog = value;
        if (this.weatherState != null)
          this.weatherState.Fog.Target = value;
      }
    }

    public float CurrentCloud
    {
      get
      {
        if (this.weatherState != null)
          return this.weatherState.Cloud.Value;
        else
          return this.WeatherManager.m_currentCloud;
      }
      internal set
      {
        this.WeatherManager.m_currentCloud = value;
        if (this.weatherState != null)
          this.weatherState.Cloud.Value = value;
      }
    }

    public float TargetCloud
    {
      get
      {
        if (this.weatherState != null)
          return this.weatherState.Cloud.Target;
        else
          return this.WeatherManager.m_targetCloud;
      }
      internal set
      {
        this.WeatherManager.m_targetCloud = value;
        if (this.weatherState != null)
          this.weatherState.Cloud.Target = value;
      }
    }

    public float CurrentRainbow
    {
      get
      {
        if (this.weatherState != null)
          return this.weatherState.Rainbow.Value;
        else
          return this.WeatherManager.m_currentRainbow;
      }
      internal set
      {
        this.WeatherManager.m_currentRainbow = value;
        if (this.weatherState != null)
          this.weatherState.Rainbow.Value = value;
      }
    }

    public float TargetRainbow
    {
      get
      {
        if (this.weatherState != null)
          return this.weatherState.Rainbow.Target;
        else
          return this.WeatherManager.m_targetRainbow;
      }
      internal set
      {
        this.WeatherManager.m_targetRainbow = value;
        if (this.weatherState != null)
          this.weatherState.Rainbow.Target = value;
      }
    }

    public float CurrentNorthernLights
    {
      get
      {
        if (this.weatherState != null)
          return this.weatherState.NorthernLights.Value;
        else
          return this.WeatherManager.m_currentNorthernLights;
      }
      internal set
      {
        this.WeatherManager.m_currentNorthernLights = value;
        if (this.weatherState != null)
          this.weatherState.NorthernLights.Value = value;
      }
    }

    public float TargetNorthernLights
    {
      get
      {
        if (this.weatherState != null)
          return this.weatherState.NorthernLights.Target;
        else
          return this.WeatherManager.m_targetNorthernLights;
      }
      internal set
      {
        this.WeatherManager.m_targetNorthernLights = value;
        if (this.weatherState != null)
          this.weatherState.NorthernLights.Target = value;
      }
    }

    #region GroundWetness Get&Set
    public float CurrentGroundWetness
    {
      get
      {
        if (this.weatherState != null)
          return this.weatherState.GroundWetness.Value;
        else
          return this.WeatherManager.m_groundWetness;
      }
      set
      {
        this.WeatherManager.m_groundWetness = value;
        if (this.weatherState != null)
          this.weatherState.GroundWetness.Value = value;
      }
    }

    public float TargetGroundWetness
    {
      get
      {
        if (this.weatherState != null)
          return this.weatherState.GroundWetness.Target;
        else
          return this.WeatherManager.m_groundWetness;
      }
      set
      {
        if (this.weatherState != null)
          this.weatherState.GroundWetness.Target = value;
        else
          this.WeatherManager.m_groundWetness = value;
      }
    }
    #endregion

    #region IClimateControlEngine implementation for Modules

    private Dictionary<string, ModuleHandling> modulesHandling = new Dictionary<string, ModuleHandling>();

    internal ModuleHandling[] RegisteredModules
    {
      get { return this.modulesHandling.Select(pair => pair.Value).ToArray(); }
    }

    private ModuleHandling ResolveModule(string moduleType, IUserMod mod)
    {
      ModuleHandling mh = new ModuleHandling() { Mod = mod };

      if (string.Equals(moduleType, ClimateControlModuleTypeIdentifiers.WaterSource, StringComparison.InvariantCultureIgnoreCase))
      {
        mh.ModuleIdent = ClimateControlModuleTypeIdentifiers.WaterSource;
        mh.ModuleName = "Water Sources";
      }

      if (string.IsNullOrEmpty(mh.ModuleIdent) || string.IsNullOrEmpty(mh.ModuleName))
        return null;

      return mh;
    }

    public void NotifyModuleHandler(string moduleType, IUserMod mod)
    {
      ModuleHandling mh = this.ResolveModule(moduleType, mod);

      if (mh != null)
      {
        Mod.GetInstance().NotifyModule(mh);
      }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="moduleType"></param>
    /// <param name="mod"></param>
    public void RegisterModuleHandler(string moduleType, IUserMod mod)
    {
      ModuleHandling mh = this.ResolveModule(moduleType, mod);

      if (mh != null)
      {
        if (modulesHandling.ContainsKey(mh.ModuleIdent) == false || modulesHandling[mh.ModuleIdent].Mod.GetType() != mod.GetType())
        {
          modulesHandling[mh.ModuleIdent] = mh;
          Mod.GetInstance().RegisterModule(mh);
        }
      }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="moduleType"></param>
    /// <param name="mod"></param>
    public void DeregisterModuleHandler(string moduleType, IUserMod mod)
    {
      ModuleHandling mh = this.ResolveModule(moduleType, mod);

      if (mh != null)
      {
        if (modulesHandling.ContainsKey(mh.ModuleIdent) && modulesHandling[mh.ModuleIdent].Mod.GetType() == mod.GetType())
        {
          modulesHandling.Remove(mh.ModuleIdent);
        }
      }
    }
    #endregion

    #region IClimateControlEngine implementation for Acquire & Disperse
    /// <summary>
    /// Attempts to acquire resource of given type in specified amount. Retrieved value is actually retrieved amount.
    /// </summary>
    /// <param name="resourceType"></param>
    /// <param name="amount"></param>
    /// <returns></returns>
    public float Acquire(string resourceType, float amount)
    {
      switch (resourceType)
      {
        case ClimateControlResourceTypeIdentifiers.SurfaceWater:
          return this.AcquireSurfaceWater(amount);
        case ClimateControlResourceTypeIdentifiers.GroundWater:
          return this.AcquireGroundWater(amount);
        case ClimateControlResourceTypeIdentifiers.OutOfMapGroundWater:
          return this.AcquireOutOfMapGroundWater(amount);
      }
      return 0;
    }

    /// <summary>
    /// Attempts to acquire resource of given type in specified amount at given position. Retrieved value is actually retrieved amount.
    /// </summary>
    /// <param name="resourceType"></param>
    /// <param name="amount"></param>
    /// <returns></returns>
    public float Acquire(string resourceType, Vector3 position, float amount)
    {
      switch (resourceType)
      {
        case ClimateControlResourceTypeIdentifiers.FlowingWater:
          return this.AcquireFlowingWater(position, amount);
      }
      return 0;
    }

    /// <summary>
    /// Attempts to disperse resource of given type in specified amount. Retrieved value is actually dispersed amount.
    /// </summary>
    /// <param name="resourceType"></param>
    /// <param name="amount"></param>
    /// <returns></returns>
    public float Disperse(string resourceType, float amount)
    {
      switch (resourceType)
      {
        case ClimateControlResourceTypeIdentifiers.SurfaceWater:
          return this.DisperseSurfaceWater(amount);
        case ClimateControlResourceTypeIdentifiers.GroundWater:
          return this.DisperseGroundWater(amount);
        case ClimateControlResourceTypeIdentifiers.OutOfMapGroundWater:
          return this.DisperseOutOfMapGroundWater(amount);
      }
      return 0;
    }

    /// <summary>
    /// Attempts to disperse resource of given type in specified amount at given position. Retrieved value is actually dispersed amount.
    /// </summary>
    /// <param name="resourceType"></param>
    /// <param name="amount"></param>
    /// <returns></returns>
    public float Disperse(string resourceType, Vector3 position, float amount)
    {
      switch (resourceType)
      {
        case ClimateControlResourceTypeIdentifiers.FlowingWater:
          return this.DisperseFlowingWater(position, amount);
      }
      return 0;
    }
    #endregion

    #region Acquire & Disperse - OutOfMapGroundWater
    /// <summary>
    /// Acquire out of map ground water from available quantity of water.
    /// </summary>
    /// <param name="quantity"></param>
    /// <returns></returns>
    private float AcquireOutOfMapGroundWater(float quantity)
    {
      float acquired = Math.Min(this.weatherState.OutOfMapGroundWater, quantity);

      this.weatherState.OutOfMapGroundWater -= acquired;

      return acquired;
    }

    /// <summary>
    /// Disperse out of map ground water to available quantity of water.
    /// </summary>
    /// <param name="quantity"></param>
    /// <returns></returns>
    private float DisperseOutOfMapGroundWater(float quantity)
    {
      this.weatherState.OutOfMapGroundWater += quantity;

      return quantity;
    }
    #endregion

    #region Acquire & Disperse - GroundWater
    /// <summary>
    /// Acquire ground water from available quantity of water.
    /// </summary>
    /// <param name="quantity"></param>
    /// <returns></returns>
    private float AcquireGroundWater(float quantity)
    {
      float acquired = Math.Min(this.weatherState.GroundWater, quantity);

      this.weatherState.GroundWater -= acquired;

      return acquired;
    }

    /// <summary>
    /// Disperse ground water to available quantity of water.
    /// </summary>
    /// <param name="quantity"></param>
    /// <returns></returns>
    private float DisperseGroundWater(float quantity)
    {
      this.weatherState.GroundWater += quantity;

      return quantity;
    }
    #endregion

    #region Acquire & Disperse - SurfaceWater
    /// <summary>
    /// Acquire surface water from available quantity of water.
    /// </summary>
    /// <param name="quantity"></param>
    /// <returns></returns>
    private float AcquireSurfaceWater(float quantity)
    {
      float acquired = Math.Min(this.weatherState.SurfaceWater, quantity);

      this.weatherState.SurfaceWater -= acquired;

      return acquired;
    }

    /// <summary>
    /// Disperse surface water to available quantity of water.
    /// </summary>
    /// <param name="quantity"></param>
    /// <returns></returns>
    private float DisperseSurfaceWater(float quantity)
    {
      this.weatherState.SurfaceWater += quantity;

      return quantity;
    }
    #endregion

    #region Acquire & Disperse - FlowingWater
    /// <summary>
    /// Acquire flowing water from available quantity of water.
    /// </summary>
    /// <param name="quantity"></param>
    /// <returns></returns>
    private float AcquireFlowingWater(Vector3 position, float quantity)
    {
      return GeneralHelper.TryGetWater(position, (ushort)quantity);
    }

    /// <summary>
    /// Disperse flowing water to available quantity of water.
    /// </summary>
    /// <param name="quantity"></param>
    /// <returns></returns>
    private float DisperseFlowingWater(Vector3 position, float quantity)
    {
      return GeneralHelper.TryAddWater(position, (ushort)quantity);
    }
    #endregion

    public float SpeedWindDirection
    {
      get
      {
        if (this.weatherState != null)
          return this.weatherState.WindDirection.Speed;
        else
          return this.WeatherManager.m_directionSpeed;
      }
      internal set
      {
        this.WeatherManager.m_directionSpeed = value;
        if (this.weatherState != null)
          this.weatherState.WindDirection.Speed = value;
      }
    }

    public float CurrentWindDirection
    {
      get
      {
        if (this.weatherState != null)
          return this.weatherState.WindDirection.Value;
        else
          return this.WeatherManager.m_windDirection;
      }
      internal set
      {
        this.WeatherManager.m_windDirection = value;
        if (this.weatherState != null)
          this.weatherState.WindDirection.Value = value;
      }
    }

    /// <summary>
    /// 0-500 range
    /// </summary>
    public float SeaLevel
    {
      get { return this.terrainManager.WaterSimulation.m_currentSeaLevel; }
      set
      {
        this.terrainManager.WaterSimulation.m_currentSeaLevel = value;
        this.terrainManager.WaterSimulation.m_nextSeaLevel = value;
      }
    }

    /// <summary>
    /// 0-24 range.
    /// </summary>
    public float CurrentSolarTimeHour
    {
      get
      {
        if (this.simulationManager != null)
          return this.simulationManager.m_currentDayTimeHour;
        else
          return 0;
      }
      set
      {
        if (this.simulationManager != null)
        {
          int num = (int)((value - this.simulationManager.m_currentDayTimeHour) / SimulationManager.DAYTIME_FRAME_TO_HOUR);
          this.simulationManager.m_dayTimeOffsetFrames = (uint)(((ulong)this.simulationManager.m_dayTimeOffsetFrames + (ulong)((long)num)) % (ulong)SimulationManager.DAYTIME_FRAMES);

          this.simulationManager.m_currentDayTimeHour = value;
        }
      }
    }

    private SavedInt temperatureUnit = new SavedInt(Settings.temperatureUnit, Settings.gameSettingsFile, 0, true);

    public string GetLocalizedTemperature(float value, int decimals = 1)
    {
      int tempUnit = (int)temperatureUnit;

      if (tempUnit == 0)
        return string.Format("{0:0." + new String('0', decimals) + "}°C", value);
      else
        return string.Format("{0:0." + new String('0', decimals) + "}°F", value * 1.79999995231628 + 32.0);
    }

    private DateTime GetGameSunTime()
    {
      int realHour = (int)this.ThreadingManager.simulationDayTimeHour;
      int realMinute = (int)((this.ThreadingManager.simulationDayTimeHour - realHour) * 60);
      return new DateTime(this.ThreadingManager.simulationTime.Year, this.ThreadingManager.simulationTime.Month, this.ThreadingManager.simulationTime.Day, realHour, realMinute, 0);
    }

    private DateTime GetRealTime()
    {
      DateTime workstationTime = DateTime.Now;
      return new DateTime(this.ThreadingManager.simulationTime.Year, this.ThreadingManager.simulationTime.Month, this.ThreadingManager.simulationTime.Day, workstationTime.Hour, workstationTime.Minute, workstationTime.Second);
    }

    private DateTime GetSimulationTime()
    {
      return this.ThreadingManager.simulationTime;
    }

    private DateTime GetRushHoursTime()
    {
      if (this.rushHoursDateTime != null)
        return (DateTime)this.rushHoursDateTime.GetValue(null);
      else
        return this.GetGameSunTime();
    }

    public DateTime GetClimateDateTime()
    {
      switch (GlobalConfig.GetInstance().TimeToUse)
      {
        case GlobalConfig.TimeType.SunTime:
          return this.GetGameSunTime();

        case GlobalConfig.TimeType.RealTime:
          return this.GetRealTime();

        case GlobalConfig.TimeType.RushHours:
          return this.GetRushHoursTime();

        case GlobalConfig.TimeType.SimulationTime:
        default:
          return this.GetSimulationTime();
      }
    }

    private bool IsNight(DateTime dt)
    {
      return dt.Hour < SimulationManager.SUNRISE_HOUR || dt.Hour > SimulationManager.SUNSET_HOUR;
    }

    private void UpdateDayNightProperties()
    {
      if (GlobalConfig.GetInstance().AlterDayLength == false)
        return;

      float simulationHour = this.simulationManager.m_dayTimeFrame * SimulationManager.DAYTIME_FRAME_TO_HOUR;

      this.simulationManager.m_isNightTime = (simulationHour < SimulationManager.SUNRISE_HOUR || simulationHour > SimulationManager.SUNSET_HOUR);

      float dayLength = SimulationManager.SUNSET_HOUR - SimulationManager.SUNRISE_HOUR;
      float dayLengthHalf = dayLength * 0.5f;
      float nightLength = 24f - dayLength;
      float nightLengthHalf = nightLength * 0.5f;

      float middayHour = SimulationManager.SUNRISE_HOUR + dayLengthHalf;
      float midnightHour = (middayHour + 12f) % 24f;

      float dayNightProperiesTimeOfDay;

      if (this.simulationManager.m_isNightTime)
      { // night
        float timeSinceSunSet = simulationHour > SimulationManager.SUNSET_HOUR ? simulationHour - SimulationManager.SUNSET_HOUR : 24f - SimulationManager.SUNSET_HOUR + simulationHour;

        if (timeSinceSunSet < nightLengthHalf)
        {
          dayNightProperiesTimeOfDay = 18 + 6 * timeSinceSunSet / nightLengthHalf;
        }
        else
        {
          dayNightProperiesTimeOfDay = 6 * (timeSinceSunSet - nightLengthHalf) / nightLengthHalf;
        }
      }
      else
      { // day
        if (simulationHour < middayHour)
        {
          dayNightProperiesTimeOfDay = 6 + 6 * (simulationHour - SimulationManager.SUNRISE_HOUR) / dayLengthHalf;
        }
        else
        {
          dayNightProperiesTimeOfDay = 12 + 6 * (simulationHour - middayHour) / dayLengthHalf;
        }
      }

      DayNightProperties.instance.m_TimeOfDay = dayNightProperiesTimeOfDay;
    }

    private ClimateFrameData currentClimateFrameData = null;
    //private MonthlyClimateData currentClimateFrameData = null;

    private DateTime lastSimulationTimeUpdate;
    private DateTime lastClimateSimulationTimeUpdate;

    private WeatherState weatherState = null;

    internal WeatherState WeatherState
    {
      get { return this.weatherState; }
      set { this.weatherState = value; }
    }

    private WeeklyStatisticData previousYearsStatisticData = null;
    private WeeklyStatisticData currentWeekStatisticData = null;

    private TempClimateData currentTempClimateData = null;

    private const float OneOverSeven = 1f / 7f;
    private const double MIN_TIME_DELTA = 15;

    public void UpdateClimate(float realTimeDelta, float simulationTimeDelta)
    {
      if (this.IsInitialized == false)
        return;

      if (!Singleton<LoadingManager>.instance.m_loadingComplete)
        return;

      //if (WeatherManager.m_enableWeather == false && WeatherManager.m_forceWeatherOn < 1.0f)
      //  return;

      ItemClass.Availability mode = Singleton<ToolManager>.instance.m_properties.m_mode;
      if ((mode & ItemClass.Availability.Game) == ItemClass.Availability.None)
        return;

      this.UpdateDayNightProperties();

      DateTime climateTime = this.GetClimateDateTime();
      bool isNight = this.IsNight(climateTime);

      DateTime simulationTime = this.ThreadingManager.simulationTime;

      TimeSpan climateSimulationTimeUpdateDelta = simulationTime - this.lastClimateSimulationTimeUpdate;
      TimeSpan simulationTimeUpdateDelta = simulationTime - this.lastSimulationTimeUpdate;

      if (this.weatherState != null)
      {
        if (simulationTimeUpdateDelta.Ticks > 0)
        {
          this.RecalculateWeatherStateSpeeds();
          this.weatherState.UpdateState(simulationTimeUpdateDelta);
        }
        this.SetValuesToWeatherManager();
      }

      this.lastSimulationTimeUpdate = simulationTime;

      if (this.weatherState == null)
      {
        this.weatherState = new WeatherState();
        this.weatherState.SetFromWeatherManager();
      }

      this.HandleThundersFrequency(climateSimulationTimeUpdateDelta);

      this.HandleWaterSourcesModule(climateSimulationTimeUpdateDelta);

      if (climateSimulationTimeUpdateDelta.TotalMinutes < MIN_TIME_DELTA)
        return;

      bool isFirstUpdateThisYear = this.lastClimateSimulationTimeUpdate.Year != simulationTime.Year;
      bool isFirstUpdateThisWeek = false;

      bool needToReinitializeWeatherProperties = false;

      if (this.HistoricalData == null)
        this.HistoricalData = new HistoryData();

      float dayProgress = (float)(climateTime.TimeOfDay.TotalHours / 24f);
      ushort weekIndex = (ushort)Mathf.FloorToInt((climateTime.DayOfYear - 1) * OneOverSeven);
      float weekProgress = (climateTime.DayOfYear - 1 + dayProgress) * OneOverSeven - weekIndex;
      float weekHours = weekProgress * 10080f;
      float yearProgress = (climateTime.DayOfYear - 1 + dayProgress) / 366f;

      if (this.currentWeekStatisticData == null || this.currentWeekStatisticData.WeekIndex != weekIndex)
      {
        isFirstUpdateThisWeek = true;

        YearlyStatisticData yearlyData = this.HistoricalData.GetYearData(climateTime, true);
        WeeklyStatisticData weeklyData = yearlyData.GetWeekData(climateTime, false);
        if (weeklyData == null)
        {
          weeklyData = yearlyData.GetWeekData(climateTime, true);
          weeklyData.TemperatureMin = weeklyData.TemperatureMax = weeklyData.TemperatureAverage = this.WeatherManager.m_currentTemperature;
        }

        this.currentWeekStatisticData = weeklyData;
        this.previousYearsStatisticData = this.HistoricalData.GetCombinedWeeklyData(weekIndex, (ushort)climateTime.Year);
      }
      // DayNightProperties - related to lightning

      // ensure that we have proper reference!
      float currentTemperature = this.WeatherManager.m_currentTemperature;
      float newTemperature = currentTemperature;

      if (this.ClimateFrames != null && this.ClimateFrames.Length > 0)
      {
        //  if(this.currentClimateFrameData != null)
        //    DebugOutputPanel.AddMessage(ColossalFramework.Plugins.PluginManager.MessageType.Message, "yp: " + yearProgress + " s/e " + this.currentClimateFrameData.YearProgressStart + "/ " + this.currentClimateFrameData.YearProgressEnd);

        if (this.currentClimateFrameData == null || this.currentClimateFrameData.YearProgressStart > yearProgress || this.currentClimateFrameData.YearProgressEnd < yearProgress)
        {
          ClimateFrameData newFrame = this.ClimateFrames[0];
          for (int i = 0; i < this.ClimateFrames.Length; i++)
          {
            ClimateFrameData frame = this.ClimateFrames[i];
            if (frame.YearProgressStart <= yearProgress && frame.YearProgressEnd >= yearProgress)
            {
              newFrame = frame;
              break;
            }
          }

          this.PrepareWeatherProperties(newFrame);

          // daytime length
          if (GlobalConfig.GetInstance().AlterDayLength)
          {
            float nightLengthAverage = this.ClimateControlProperties.SolarDayLength - newFrame.DayLengthAverage;

            float midday = this.ClimateControlProperties.SolarDayLength * 0.5f;

            float sunRise = midday - newFrame.DayLengthAverage * 0.5f - SimulationManager.DAYTIME_FRAME_TO_HOUR;
            float sunSet = midday + newFrame.DayLengthAverage * 0.5f + SimulationManager.DAYTIME_FRAME_TO_HOUR;

            SimulationManager.RELATIVE_DAY_LENGTH = Mathf.RoundToInt(newFrame.DayLengthAverage / 3);
            SimulationManager.RELATIVE_NIGHT_LENGTH = Mathf.RoundToInt(nightLengthAverage / 3);

            SimulationManager.SUNRISE_HOUR = Mathf.Min(24f - SimulationManager.DAYTIME_FRAME_TO_HOUR, Mathf.Max(SimulationManager.DAYTIME_FRAME_TO_HOUR, sunRise));
            SimulationManager.SUNSET_HOUR = Mathf.Min(24f - SimulationManager.DAYTIME_FRAME_TO_HOUR, Mathf.Max(SimulationManager.DAYTIME_FRAME_TO_HOUR, sunSet));

            //DayNightProperties.instance.m_TimeOfDay = SimulationManager.Lagrange4(simHour, 0f, SimulationManager.SUNRISE_HOUR, SimulationManager.SUNSET_HOUR, 24f, 0f, 6f, 18f, 24f);

            // Range: <-6,6>
            //Singleton<RenderManager>.instance.lightSystem.SetDaylightIntensity(this.m_TimeOfDay - 6f);

            //SimulationManager.SYNCHRONIZE_TIMEOUT = 0;
            //SimulationManager.DAYTIME_FRAMES = 65536u;
            //SimulationManager.DAYTIME_FRAME_TO_HOUR = 24f / SimulationManager.DAYTIME_FRAMES;
            //SimulationManager.DAYTIME_HOUR_TO_FRAME = SimulationManager.DAYTIME_FRAMES / 24f;
            //SimulationManager.SUNRISE_HOUR = 5f;
            //SimulationManager.SUNSET_HOUR = 20f;
            //SimulationManager.RELATIVE_DAY_LENGTH = 5;
            //SimulationManager.RELATIVE_NIGHT_LENGTH = 3;
            //SimulationManager.SIMULATION_PRIORITY = ThreadPriority.Normal;
            //SimulationManager.m_managers = new FastList<ISimulationManager>();
          }
          else
          {
            SimulationManager.RELATIVE_DAY_LENGTH = 5;
            SimulationManager.RELATIVE_NIGHT_LENGTH = 3;
            SimulationManager.SUNRISE_HOUR = 5f;
            SimulationManager.SUNSET_HOUR = 20f;
          }

          if (GlobalConfig.GetInstance().ChirpForecast)
          {
            ChirpBox.SendMessage("Weather Forecast", string.Format("Forecast for next {0:0} days:" + Environment.NewLine + "Day {1} - {2}" + Environment.NewLine + "Night {3} - {4}",
              newFrame.YearProgressDayLength,
              this.GetLocalizedTemperature(this.CurrentWeatherProperties.m_minTemperatureDay),
              this.GetLocalizedTemperature(this.CurrentWeatherProperties.m_maxTemperatureDay),
              this.GetLocalizedTemperature(this.CurrentWeatherProperties.m_minTemperatureNight),
              this.GetLocalizedTemperature(this.CurrentWeatherProperties.m_maxTemperatureNight)
              ));
          }

          this.currentClimateFrameData = newFrame;
          needToReinitializeWeatherProperties = true;
        }

        if (isFirstUpdateThisWeek)
        {
          this.currentTempClimateData = new TempClimateData();
          this.currentTempClimateData.PrecipitationHoursExpected = this.currentClimateFrameData.PrecipitationDaysRatio * 7 * this.ClimateControlProperties.SolarDayLength * 0.4f;

          this.currentTempClimateData.PrecipitationAverage = this.currentClimateFrameData.PrecipitationAverage * this.random.Next(5, 15) * 0.1f;
          this.currentTempClimateData.PrecipitationExpected = this.currentTempClimateData.PrecipitationAverage * 7;

          this.currentTempClimateData.FogHoursExpected = this.currentClimateFrameData.FogDaysRatio * 7 * this.ClimateControlProperties.SolarDayLength * 0.4f;
        }

        float currentFrameProgress = (yearProgress - this.currentClimateFrameData.YearProgressStart) / this.currentClimateFrameData.YearProgressLength;

        // Temperature
        // Affected by: Clouds, Wind
        if (GlobalConfig.GetInstance().AlterTemperature)
        {
          float minTemp;
          float maxTemp;

          if (isNight)
          {
            minTemp = this.CurrentWeatherProperties.m_minTemperatureNight;
            maxTemp = this.CurrentWeatherProperties.m_maxTemperatureNight;
          }
          else
          {
            minTemp = this.CurrentWeatherProperties.m_minTemperatureDay;
            maxTemp = this.CurrentWeatherProperties.m_maxTemperatureDay;
          }

          float revRain = 1.0f - this.weatherState.Rain.Value;
          float revFog = 1.0f - this.weatherState.Fog.Value;

          minTemp =
            this.CurrentWeatherProperties.m_minTemperatureRain * this.weatherState.Rain.Value
            + this.CurrentWeatherProperties.m_minTemperatureFog * this.weatherState.Fog.Value
            + minTemp * (revRain + revFog);
          minTemp *= 0.5f;

          maxTemp =
            this.CurrentWeatherProperties.m_maxTemperatureRain * this.weatherState.Rain.Value
            + this.CurrentWeatherProperties.m_maxTemperatureFog * this.weatherState.Fog.Value
            + maxTemp * (revRain + revFog);
          maxTemp *= 0.5f;

          float cloudFactor = this.weatherState.Cloud.Value * 5.0f;
          minTemp -= cloudFactor;
          maxTemp -= cloudFactor;

          minTemp = Mathf.Clamp(minTemp, -273.15f, 30000.0f);
          maxTemp = Mathf.Clamp(maxTemp, -273.15f, 30000.0f);

          if (this.weatherState.Temperature.Target > maxTemp + 5f || this.weatherState.Temperature.Target < minTemp - 5f
            || this.simulationManager.m_randomizer.Int32(175u) == 0)
          {
            float newTarget = (float)random.NextDouble() * (maxTemp - minTemp) + minTemp;

            this.weatherState.Temperature.Target = newTarget;
          }
        }

        #region Vanilla handling
        /*
        float num8 = Mathf.Abs((this.m_targetTemperature - this.m_currentTemperature) * 0.001f);
        this.m_temperatureSpeed = Mathf.Min(this.m_temperatureSpeed + 0.0001f, num8);
        this.m_currentTemperature = Mathf.MoveTowards(this.m_currentTemperature, this.m_targetTemperature, this.m_temperatureSpeed);
        int dayTimeFrame = (int)instance.m_dayTimeFrame;
        int dAYTIME_FRAMES = (int)SimulationManager.DAYTIME_FRAMES;
        float t = Mathf.Abs((float)dayTimeFrame / (float)dAYTIME_FRAMES - 0.5f) * 2f;
        float num9 = Mathf.Lerp(this.m_properties.m_minTemperatureDay, this.m_properties.m_minTemperatureNight, t);
        float num10 = Mathf.Lerp(this.m_properties.m_maxTemperatureDay, this.m_properties.m_maxTemperatureNight, t);
        num9 = Mathf.Lerp(num9, this.m_properties.m_minTemperatureRain, this.m_currentRain);
        num10 = Mathf.Lerp(num10, this.m_properties.m_maxTemperatureRain, this.m_currentRain);
        num9 = Mathf.Lerp(num9, this.m_properties.m_minTemperatureFog, this.m_currentFog);
        num10 = Mathf.Lerp(num10, this.m_properties.m_maxTemperatureFog, this.m_currentFog);
        if (num8 < 2E-05f || this.m_targetTemperature < num9 || this.m_targetTemperature > num10)
        {
          this.m_targetTemperature = (float)instance.m_randomizer.Int32(0, 10000) * (num10 - num9) / 10000f + num9;
        }
        */
        #endregion

        // Rain
        // Affected by: Clouds
        if (GlobalConfig.GetInstance().AlterRain)
        {
          if (this.simulationManager.m_randomizer.Int32(150u) == 0)
          {
            float probability = isNight ? this.CurrentWeatherProperties.m_rainProbabilityNight : this.CurrentWeatherProperties.m_rainProbabilityDay;
            probability *= 0.01f;

            // there should be clouds when it rains
            probability += weatherState.Cloud.Value * 0.15f;
            if (this.currentTempClimateData.PrecipitationExpected > this.currentWeekStatisticData.PrecipitationAmount)
              probability *= 1.35f;
            else
              probability *= 0.85f;

            if (this.currentTempClimateData.PrecipitationHoursExpected > this.currentWeekStatisticData.PrecipitationDuration)
              probability *= 1.55f;
            else
              probability *= 0.65f;

            float chance = probability - (float)random.NextDouble();

            float newTarget;
            if (chance <= 0.01f)
              newTarget = WeatherState.RainMinValue;
            else
            {
              chance = (chance + probability) * 0.75f;

              // rain drain clouds
              chance -= (1.0f - weatherState.Cloud.Value) * 0.05f;

              float precipitationToFill = (this.currentTempClimateData.PrecipitationExpected - this.currentWeekStatisticData.PrecipitationAmount);

              float daysToNextWeek = (1f - weekProgress) * 7f;

              precipitationToFill = (precipitationToFill / (daysToNextWeek * this.ClimateControlProperties.SolarDayLength));
              // 1 to 4!
              precipitationToFill *= 0.25f;

              newTarget = Mathf.Clamp(chance + precipitationToFill, WeatherState.RainMinValue, WeatherState.RainMaxValue);
            }

            this.weatherState.Rain.Target = newTarget;
          }
        }

        #region Vanilla handling
        /*
        if (this.m_targetRain > this.m_currentRain)
        {
          float currentRain = this.m_currentRain;
          this.m_currentRain = Mathf.Min(this.m_targetRain, this.m_currentRain + 0.0002f);
        }
        else if (this.m_targetRain < this.m_currentRain)
        {
          this.m_currentRain = Mathf.Max(this.m_targetRain, this.m_currentRain - 0.0002f);
        }
        else if (instance.m_randomizer.Int32(20000u) == 0)
        {
          if (this.m_currentFog == 0f && this.m_targetFog == 0f)
          {
            int num3 = (!instance.m_isNightTime) ? this.m_properties.m_rainProbabilityDay : this.m_properties.m_rainProbabilityNight;
            if (instance.m_randomizer.Int32(100u) < num3)
            {
              this.m_targetRain = (float)instance.m_randomizer.Int32(2500, 10000) * 0.0001f;
            }
            else
            {
              this.m_targetRain = 0f;
            }
          }
          else
          {
            this.m_targetRain = 0f;
          }
        }
        */
        #endregion

        // Fog
        // Affected by: Rain, GroundWetness, Temperature
        if (GlobalConfig.GetInstance().AlterFog)
        {
          if (this.simulationManager.m_randomizer.Int32(150u) == 0)
          {
            float probability = isNight ? this.CurrentWeatherProperties.m_fogProbabilityNight : this.CurrentWeatherProperties.m_fogProbabilityDay;
            probability *= 0.01f;

            // rain removes fog a little
            probability -= weatherState.Rain.Value * 0.05f;
            // wet ground makes fog easier to appear
            probability += weatherState.GroundWetness.Value * 0.15f;

            if (weatherState.Temperature.Value > this.ClimateControlProperties.SnowMeltTemperature + 40.0f)
              probability *= 0.35f;
            else if (weatherState.Temperature.Value > this.ClimateControlProperties.SnowMeltTemperature + 20.0f)
              probability *= 0.75f;
            else if (weatherState.Temperature.Value > this.ClimateControlProperties.SnowMeltTemperature)
              probability *= 0.99f;
            else
              probability *= 0.85f;

            if (this.currentTempClimateData.FogHoursExpected > this.currentWeekStatisticData.FogDuration)
              probability *= 1.50f;
            else
              probability *= 0.50f;

            float chance = probability - (float)random.NextDouble();

            float newTarget;
            if (chance <= 0.05f)
              newTarget = 0.0f;
            else
            {
              chance = (chance + probability) * 0.75f;
              newTarget = Mathf.Clamp01(chance);
            }

            this.weatherState.Fog.Target = newTarget;
          }
        }

        #region Vanilla handling
        /*
        if (this.m_targetFog > this.m_currentFog)
        {
          float currentFog = this.m_currentFog;
          this.m_currentFog = Mathf.Min(this.m_targetFog, this.m_currentFog + 0.0002f);
          if (this.m_currentFog >= 0.2f && currentFog < 0.2f && Singleton<SimulationManager>.instance.m_metaData.m_disableAchievements != SimulationMetaData.MetaBool.True)
          {
            ThreadHelper.dispatcher.Dispatch(delegate
            {
              if (!PlatformService.achievements["FoggyWeather"].achieved)
              {
                PlatformService.achievements["FoggyWeather"].Unlock();
              }
            });
          }
        }
        else if (this.m_targetFog < this.m_currentFog)
        {
          this.m_currentFog = Mathf.Max(this.m_targetFog, this.m_currentFog - 0.0002f);
        }
        else if (instance.m_randomizer.Int32(20000u) == 0)
        {
          if (this.m_currentRain == 0f && this.m_targetRain == 0f)
          {
            int num4 = (!instance.m_isNightTime) ? this.m_properties.m_fogProbabilityDay : this.m_properties.m_fogProbabilityNight;
            if (instance.m_randomizer.Int32(100u) < num4)
            {
              this.m_targetFog = (float)instance.m_randomizer.Int32(2500, 10000) * 0.0001f;
            }
            else
            {
              this.m_targetFog = 0f;
            }
          }
          else
          {
            this.m_targetFog = 0f;
          }
        }
        */
        #endregion

        // GroundWetness
        // Affected by: Rain, Temperature, Fog, Clouds
        if (GlobalConfig.GetInstance().AlterGroundWetness)
        {
          float newTarget = this.weatherState.GroundWetness.Value;

          if (weatherState.Temperature.Value > this.ClimateControlProperties.SnowMeltTemperature + 40.0f)
            newTarget *= 0.15f;
          else if (weatherState.Temperature.Value > this.ClimateControlProperties.SnowMeltTemperature + 20.0f)
            newTarget *= 0.55f;
          else if (weatherState.Temperature.Value > this.ClimateControlProperties.SnowMeltTemperature)
            newTarget *= 0.80f;
          else
            newTarget *= 0.95f;

          newTarget += this.weatherState.Rain.Value * 0.5f;

          if (this.weatherState.Rain.Value > 0.0f)
            newTarget *= 1.05f;

          this.weatherState.GroundWetness.Target = Mathf.Clamp01(newTarget);
        }

        #region Vanilla handling
        /*
        float num6 = this.m_groundWetness;
        num6 -= num6 * 0.00013f + 1E-05f;
        num6 += Mathf.Min(this.m_currentRain, 0.25f) * 0.00061f;
        this.m_groundWetness = Mathf.Clamp01(num6);
        */
        #endregion

        // Cloud
        // Affected by: Rain, Fog, GroundWetness, Temperature
        if (GlobalConfig.GetInstance().AlterClouds)
        {
          if (this.simulationManager.m_randomizer.Int32(100u) == 0)
          {
            float newTarget;
            float probability = isNight ? this.CurrentWeatherProperties.m_cloudProbabilityNight : this.CurrentWeatherProperties.m_cloudProbabilityDay;
            probability *= 0.01f;

            // there should be clouds when it rains
            probability += weatherState.Rain.Value * 0.65f;
            probability += weatherState.Fog.Value * 0.10f;

            if (weatherState.Temperature.Value > this.ClimateControlProperties.SnowMeltTemperature + 20.0f)
              probability += weatherState.GroundWetness.Value * 0.15f;
            else if (weatherState.Temperature.Value > this.ClimateControlProperties.SnowMeltTemperature)
              probability += weatherState.GroundWetness.Value * 0.05f;
            else
              probability += weatherState.GroundWetness.Value * 0.01f;

            float chance = probability - (float)random.NextDouble();

            if (chance <= 0)
              newTarget = 0.0f;
            else
            {
              chance = (chance + probability) * 0.75f;
              // rain drain clouds
              chance -= weatherState.Rain.Value * 0.20f;
              newTarget = Mathf.Clamp01(chance * 1.1f);
            }

            this.weatherState.Cloud.Target = newTarget;
          }
        }

        #region Vanilla handling
        /*
        if (this.m_targetCloud > this.m_currentCloud)
        {
          this.m_currentCloud = Mathf.Min(this.m_targetCloud, this.m_currentCloud + 0.0008f);
        }
        else if (this.m_targetCloud < this.m_currentCloud)
        {
          this.m_currentCloud = Mathf.Max(this.m_targetCloud, this.m_currentCloud - 0.0008f);
        }
        else if (instance.m_randomizer.Int32(20000u) == 0)
        {
          int num5 = (!instance.m_isNightTime) ? this.m_properties.m_cloudProbabilityDay : this.m_properties.m_cloudProbabilityNight;
          if (instance.m_randomizer.Int32(100u) < num5)
          {
            this.m_targetCloud = (float)instance.m_randomizer.Int32(2500, 10000) * 0.0001f;
          }
          else
          {
            this.m_targetCloud = 0f;
          }
        }
        */
        #endregion

        // Rainbow
        // Affected by: Clouds, Rain, Daytime
        if (GlobalConfig.GetInstance().AlterRainbow)
        {
          if (isNight)
          {
            this.weatherState.Rainbow.Target = 0.0f;
          }
          else
          {
            float possibleRainbow = (1.0f - this.weatherState.Cloud.Value) * 1.2f;
            float newTarget = Math.Min(this.weatherState.Rain.Value * 2, possibleRainbow);

            if (newTarget * 100 > 100 - this.CurrentWeatherProperties.m_rainbowProbability)
              newTarget *= 2.0f;
            else
              newTarget = 0.0f;
            this.weatherState.Rainbow.Target = Mathf.Clamp01(newTarget);
          }
        }

        #region Vanilla handling
        /*
        if (this.m_groundWetness == 0f && this.m_currentRain != 0f)
        {
          this.m_targetRainbow = ((instance.m_randomizer.Int32(100u) >= this.m_properties.m_rainbowProbability) ? 0f : 1f);
        }
        float num7 = Mathf.Min(1f - this.m_currentRain * 4f, this.m_groundWetness);
        num7 = Mathf.Clamp01(Mathf.Min(num7, this.m_targetRainbow));
        if (num7 > this.m_currentRainbow)
        {
          this.m_currentRainbow = Mathf.Min(num7, this.m_currentRainbow + 0.0002f);
        }
        else if (num7 < this.m_currentRainbow)
        {
          this.m_currentRainbow = Mathf.Max(num7, this.m_currentRainbow - 0.0002f);
        }
        */
        #endregion

        // NorthernLights
        // Affected by: ???
        if (GlobalConfig.GetInstance().AlterNorthernLights)
        {
          if (this.simulationManager.m_randomizer.Int32(50u) == 0)
          {
            float newTarget;

            if (random.Next(0, 100) <= this.CurrentWeatherProperties.m_northernLightsProbability)
              newTarget = ((float)random.NextDouble() * 0.95f + 0.05f) * 100f;
            else
              newTarget = 0.0f;

            this.weatherState.NorthernLights.Target = newTarget;
          }
        }

        #region Vanilla handling
        /*
        if (this.m_targetNorthernLights > this.m_currentNorthernLights)
        {
          this.m_currentNorthernLights = Mathf.Min(this.m_targetNorthernLights, this.m_currentNorthernLights + 0.0002f);
        }
        else if (this.m_targetNorthernLights < this.m_currentRain)
        {
          this.m_currentNorthernLights = Mathf.Max(this.m_targetNorthernLights, this.m_currentNorthernLights - 0.0002f);
        }
        else if (instance.m_randomizer.Int32(10000u) == 0)
        {
          if (instance.m_randomizer.Int32(100u) < this.m_properties.m_northernLightsProbability)
          {
            this.m_targetNorthernLights = (float)instance.m_randomizer.Int32(5000, 10000) * 0.0001f;
          }
          else
          {
            this.m_targetNorthernLights = 0f;
          }
        }
        */
        #endregion

        // Wind Direction
        // Affected by: ???
        if (GlobalConfig.GetInstance().AlterWindDirection)
        {
          if (float.IsNaN(this.weatherState.WindDirection.Value))
            this.weatherState.WindDirection.Value = 0;
          if (float.IsNaN(this.weatherState.WindDirection.Target))
            this.weatherState.WindDirection.Target = 0;
          if (float.IsNaN(this.weatherState.WindDirection.Speed))
            this.weatherState.WindDirection.Speed = 0;

          if (MathBetter.AngleDelta(this.weatherState.WindDirection.Value, this.weatherState.WindDirection.Target) < 0.2f
            || random.NextDouble() < 0.02)
          {
            float newTarget = this.weatherState.WindDirection.Target;
            newTarget += ((float)random.NextDouble() - 0.5f) * 90.0f;

            this.weatherState.WindDirection.Target = MathBetter.Mod(newTarget, 360.0f); ;
          }
        }

        #region Vanilla handling
        /*
        SimulationManager instance = Singleton<SimulationManager>.instance;
        float num = Mathf.Abs(Mathf.DeltaAngle(this.m_targetDirection, this.m_windDirection) * 0.001f);
        this.m_directionSpeed = Mathf.Min(this.m_directionSpeed + 0.001f, num);
        this.m_windDirection = Mathf.MoveTowardsAngle(this.m_windDirection, this.m_targetDirection, this.m_directionSpeed);
        this.m_windDirection = Mathf.DeltaAngle(0f, this.m_windDirection);
        if (num < 0.0002f)
        {
          this.m_targetDirection = (float)instance.m_randomizer.Int32(10000u) * 0.036f;
        }
        */
        #endregion

        float waterTransfer;
        if (this.weatherState.SurfaceWater > 0)
        {
          waterTransfer = Math.Min(this.weatherState.SurfaceWater, (float)(86400 * climateSimulationTimeUpdateDelta.TotalMinutes));
          this.weatherState.SurfaceWater -= waterTransfer;
          this.weatherState.GroundWater += waterTransfer;

          if (weatherState.Temperature.Value > this.ClimateControlProperties.SnowMeltTemperature + 20.0f)
            waterTransfer = 0.01f;
          else if (weatherState.Temperature.Value > this.ClimateControlProperties.SnowMeltTemperature)
            waterTransfer = 0.001f;
          else
            waterTransfer = 0.0001f;
          waterTransfer = waterTransfer * this.weatherState.SurfaceWater * (float)climateSimulationTimeUpdateDelta.TotalMinutes;
          this.weatherState.SurfaceWater -= waterTransfer;
          this.weatherState.AirWater += waterTransfer;
        }

        if (this.weatherState.GroundWater > 0)
        {
          // what ground water can do?... nothing...
          waterTransfer = Math.Min(this.weatherState.GroundWater, (float)(3600 * climateSimulationTimeUpdateDelta.TotalMinutes));
          this.weatherState.GroundWater -= waterTransfer;
          this.weatherState.OutOfMapGroundWater += waterTransfer;
        }

        if (this.weatherState.OutOfMapGroundWater > 0)
        {
          waterTransfer = Math.Min(this.weatherState.OutOfMapGroundWater, (float)(900 * climateSimulationTimeUpdateDelta.TotalMinutes));
          this.weatherState.OutOfMapGroundWater -= waterTransfer;
          this.weatherState.GroundWater += waterTransfer;

          waterTransfer = Math.Min(this.weatherState.OutOfMapGroundWater, (float)(450 * climateSimulationTimeUpdateDelta.TotalMinutes));
          this.weatherState.OutOfMapGroundWater -= waterTransfer;
          this.weatherState.SurfaceWater += waterTransfer;
        }

        if (this.weatherState.AirWater > 0)
        {

        }

        if (this.weatherState.CloudsWater > 0)
        {

        }

        // Ground related water exchange


        // Air related water exchange



        // Out of Map water equalizers 64 : 1
        float desiredGroundWaterToMatchAirWater = this.weatherState.OutOfMapAirWater * 64f;
        if (this.weatherState.OutOfMapGroundWater < desiredGroundWaterToMatchAirWater)
        {
          waterTransfer = Math.Min(desiredGroundWaterToMatchAirWater - this.weatherState.OutOfMapGroundWater, this.weatherState.OutOfMapAirWater);
          waterTransfer = Math.Min(waterTransfer, 300 * (float)climateSimulationTimeUpdateDelta.TotalMinutes);

          this.weatherState.OutOfMapAirWater -= waterTransfer;
          this.weatherState.OutOfMapGroundWater += waterTransfer;
        }
        else
        {
          if (weatherState.Temperature.Value > this.ClimateControlProperties.SnowMeltTemperature + 20.0f)
            waterTransfer = 0.05f;
          else if (weatherState.Temperature.Value > this.ClimateControlProperties.SnowMeltTemperature)
            waterTransfer = 0.005f;
          else
            waterTransfer = 0.0005f;
          waterTransfer = waterTransfer * this.weatherState.OutOfMapGroundWater * (float)climateSimulationTimeUpdateDelta.TotalMinutes;
          waterTransfer = Math.Min(waterTransfer, this.weatherState.OutOfMapGroundWater - desiredGroundWaterToMatchAirWater);

          this.weatherState.OutOfMapGroundWater -= waterTransfer;
          this.weatherState.OutOfMapAirWater += waterTransfer;
        }
      }
      else
      {
        // if no frames are provided then weather state should be same as weather manager state
        this.weatherState.SetFromWeatherManager();
      }

      //this.SetValuesToWeatherManager();

      // rain/snow sets - keep it at the end!
      bool rainIsSnow = this.WeatherManager.m_currentTemperature < this.ClimateControlProperties.SnowFallTemperature;
      if (rainIsSnow != this.CurrentWeatherProperties.m_rainIsSnow)
      {
        this.CurrentWeatherProperties.m_rainIsSnow = rainIsSnow;
        needToReinitializeWeatherProperties = true;

        // Reset rain particles behavior
        this.HandlePrecipitationParticles();
      }

      // does snow melt?
      if (this.WeatherManager.m_currentTemperature > this.ClimateControlProperties.SnowMeltTemperature)
      {
        if (this.netManager.m_treatWetAsSnow == true)
        {
          // this instantly melt "road" snow away!
          this.netManager.m_treatWetAsSnow = false;
        }
      }
      else
      {
        if (this.netManager.m_treatWetAsSnow == false && rainIsSnow)
        {
          this.netManager.m_treatWetAsSnow = true;
        }
      }

      if (GlobalConfig.GetInstance().AlterSnowDumpSnowMelting)
      {
        this.HandleSnowDumps(climateSimulationTimeUpdateDelta);
      }

      if (GlobalConfig.GetInstance().RainfallMakesWater)
      {
        this.HandleRainfallMakesWater(climateSimulationTimeUpdateDelta);
      }

      if (needToReinitializeWeatherProperties)
      {
        this.WeatherManager.InitializeProperties(this.CurrentWeatherProperties);
      }

      // Handle historical data
      this.currentWeekStatisticData.TemperatureMin = Mathf.Min(this.currentWeekStatisticData.TemperatureMin, this.WeatherManager.m_currentTemperature);
      this.currentWeekStatisticData.TemperatureMax = Mathf.Max(this.currentWeekStatisticData.TemperatureMax, this.WeatherManager.m_currentTemperature);

      float timeUsedForStatisticData = Mathf.Clamp((float)(weekHours - MIN_TIME_DELTA), 0f, 10080f); //10080 = that many minutes you get in week
      this.currentWeekStatisticData.TemperatureAverage = (float)((this.currentWeekStatisticData.TemperatureAverage * timeUsedForStatisticData) + this.WeatherManager.m_currentTemperature * MIN_TIME_DELTA) / weekHours;

      if (this.WeatherManager.m_currentRain > 0)
      {
        this.currentWeekStatisticData.PrecipitationDuration += (float)climateSimulationTimeUpdateDelta.TotalHours;

        float rainfallInOfWaterColumn = this.WeatherManager.m_currentRain * (float)climateSimulationTimeUpdateDelta.TotalHours;
        this.currentWeekStatisticData.PrecipitationAmount += rainfallInOfWaterColumn;// rainfallInMetersOfWaterColumn * 0.001f;// (float)(this.WeatherManager.m_currentRain * climateSimulationTimeUpdateDelta.TotalHours * 4f);
      }

      if (this.WeatherManager.m_currentFog > 0)
      {
        this.currentWeekStatisticData.FogDuration += (float)climateSimulationTimeUpdateDelta.TotalHours;
      }

      this.lastClimateSimulationTimeUpdate = simulationTime;
    }

    private void RecalculateWeatherStateSpeeds()
    {
      if (this.weatherState == null)
        return;

      if (GlobalConfig.GetInstance().AlterTemperature)
      {
        this.weatherState.Temperature.ApplySpeedAcceleration(0.0001f, 0.001f);
      }

      if (GlobalConfig.GetInstance().AlterRain)
      {
        this.weatherState.Rain.ApplySpeedAcceleration(0.00001f, 0.0005f);
      }

      if (GlobalConfig.GetInstance().AlterFog)
      {
        this.weatherState.Fog.ApplySpeedAcceleration(0.00001f, 0.0005f);
      }

      if (GlobalConfig.GetInstance().AlterGroundWetness)
      {
        this.weatherState.GroundWetness.ApplySpeedAcceleration(0.000001f, 0.000005f);
      }

      if (GlobalConfig.GetInstance().AlterClouds)
      {
        this.weatherState.Cloud.ApplySpeedAcceleration(0.00001f, 0.0005f);
      }

      if (GlobalConfig.GetInstance().AlterRainbow)
      {
        this.weatherState.Rainbow.ApplySpeedAcceleration(0.0001f, 0.001f);
      }

      if (GlobalConfig.GetInstance().AlterNorthernLights)
      {
        this.weatherState.NorthernLights.ApplySpeedAcceleration(0.00001f, 0.0005f);
      }

      if (GlobalConfig.GetInstance().AlterWindDirection)
      {
        this.weatherState.WindDirection.ApplyAngleSpeedAcceleration(0.005f, 0.003f);
      }
    }

    /// <summary>
    /// Sets 
    /// </summary>
    /// <param name="frame"></param>
    private void PrepareWeatherProperties(ClimateFrameData frame)
    {
      this.WeatherManager.m_enableWeather = true;
      this.WeatherManager.m_forceWeatherOn += 1.0f;

      // Temperatures
      float averageToHigh = frame.TemperatureHighAverage - frame.TemperatureAverage;
      float averageToLow = frame.TemperatureAverage - frame.TemperatureLowAverage;

      // double averages for greater temp variety!
      averageToHigh *= 2;
      averageToLow *= 2;

      float dayLow = frame.TemperatureHighAverage - averageToHigh * (float)this.random.NextDouble();
      float dayHigh = frame.TemperatureHighAverage + averageToHigh * (float)this.random.NextDouble();
      float nightLow = frame.TemperatureLowAverage - averageToLow * (float)this.random.NextDouble();
      float nightHigh = frame.TemperatureLowAverage + averageToLow * (float)this.random.NextDouble();

      // Normal Temperature
      this.CurrentWeatherProperties.m_minTemperatureDay = Mathf.Max(frame.TemperatureLowest, dayLow);
      this.CurrentWeatherProperties.m_maxTemperatureDay = Mathf.Min(frame.TemperatureHighest, dayHigh);
      this.CurrentWeatherProperties.m_minTemperatureNight = Mathf.Max(frame.TemperatureLowest, nightLow);
      this.CurrentWeatherProperties.m_maxTemperatureNight = Mathf.Min(frame.TemperatureHighest, nightHigh);

      // Rain Temperature
      this.CurrentWeatherProperties.m_minTemperatureRain = Mathf.Min(this.CurrentWeatherProperties.m_minTemperatureDay, this.CurrentWeatherProperties.m_minTemperatureNight);
      this.CurrentWeatherProperties.m_maxTemperatureRain = Mathf.Max(this.CurrentWeatherProperties.m_maxTemperatureDay, this.CurrentWeatherProperties.m_maxTemperatureNight);

      // Fog Temperature
      this.CurrentWeatherProperties.m_minTemperatureFog = this.CurrentWeatherProperties.m_minTemperatureRain;
      this.CurrentWeatherProperties.m_maxTemperatureFog = this.CurrentWeatherProperties.m_maxTemperatureRain;

      // Rain
      this.CurrentWeatherProperties.m_rainProbabilityDay = (int)(frame.PrecipitationDaysRatio * 40);
      this.CurrentWeatherProperties.m_rainProbabilityNight = (int)(frame.PrecipitationDaysRatio * 60);

      // Fog
      this.CurrentWeatherProperties.m_fogProbabilityDay = (int)(frame.FogDaysRatio * 60);
      this.CurrentWeatherProperties.m_fogProbabilityNight = (int)(frame.FogDaysRatio * 40);

      // Rainbow
      this.CurrentWeatherProperties.m_rainbowProbability = random.Next(5, 95);

      // Norther Lights
      this.CurrentWeatherProperties.m_northernLightsProbability = random.Next(5, 95);

      // Clouds
      this.CurrentWeatherProperties.m_cloudProbabilityDay = random.Next(15, 85);
      this.CurrentWeatherProperties.m_cloudProbabilityNight = random.Next(15, 85);

      // wind speed range 0f - 2f
      // sampled wind speed range 0f - 1f

      // sun intensity...
      //float num = (float)Singleton<SimulationManager>.instance.m_dayTimeFrame * SimulationManager.DAYTIME_FRAME_TO_HOUR;
      //return Mathf.Clamp01((float)(0.5 + (double)Mathf.Min(num - SimulationManager.SUNRISE_HOUR, SimulationManager.SUNSET_HOUR - num) * 2.0)) * (float)(1.0 - (double)Mathf.Max(this.m_currentRain, this.m_currentFog) * 0.5);
    }

    private void SetValuesToWeatherManager()
    {
      if (this.FixedRainbow.HasValue)
        this.WeatherManager.m_currentRainbow = this.FixedRainbow.Value;
      else if (GlobalConfig.GetInstance().AlterRainbow)
      {
        this.WeatherManager.m_currentRainbow = this.weatherState.Rainbow.Value;
        this.WeatherManager.m_targetRainbow = this.weatherState.Rainbow.Target;
      }

      if (this.FixedNorthernLights.HasValue)
        this.WeatherManager.m_currentNorthernLights = this.FixedNorthernLights.Value;
      else if (GlobalConfig.GetInstance().AlterNorthernLights)
      {
        this.WeatherManager.m_currentNorthernLights = this.weatherState.NorthernLights.Value;
        this.WeatherManager.m_targetNorthernLights = this.weatherState.NorthernLights.Target;
      }

      if (this.FixedGroundWetness.HasValue)
        this.WeatherManager.m_currentRainbow = this.FixedGroundWetness.Value;
      else if (GlobalConfig.GetInstance().AlterGroundWetness)
        this.WeatherManager.m_groundWetness = this.weatherState.GroundWetness.Value;

      if (this.FixedCloud.HasValue)
        this.WeatherManager.m_currentCloud = this.FixedCloud.Value;
      else if (GlobalConfig.GetInstance().AlterClouds)
      {
        this.WeatherManager.m_currentCloud = this.weatherState.Cloud.Value;
        this.WeatherManager.m_targetCloud = this.weatherState.Cloud.Target;
      }

      if (this.FixedFog.HasValue)
        this.WeatherManager.m_currentFog = this.FixedFog.Value;
      else if (GlobalConfig.GetInstance().AlterFog)
      {
        this.WeatherManager.m_currentFog = this.weatherState.Fog.Value;
        this.WeatherManager.m_targetFog = this.weatherState.Fog.Target;
      }

      if (this.FixedRain.HasValue)
        this.WeatherManager.m_currentRain = this.FixedRain.Value;
      else if (GlobalConfig.GetInstance().AlterRain)
      {
        this.WeatherManager.m_currentRain = this.weatherState.Rain.Value;
        this.WeatherManager.m_targetRain = this.weatherState.Rain.Target;
      }

      if (this.FixedTemperature.HasValue)
        this.WeatherManager.m_currentTemperature = this.FixedTemperature.Value;
      else if (GlobalConfig.GetInstance().AlterTemperature)
      {
        this.WeatherManager.m_currentTemperature = this.weatherState.Temperature.Value;
        this.WeatherManager.m_targetTemperature = this.weatherState.Temperature.Target;
        this.WeatherManager.m_temperatureSpeed = this.weatherState.Temperature.Speed;
      }

      if (this.FixedWindDirection.HasValue)
        this.WeatherManager.m_windDirection = this.FixedWindDirection.Value;
      else if (GlobalConfig.GetInstance().AlterWindDirection)
      {
        this.WeatherManager.m_windDirection = this.weatherState.WindDirection.Value;
        this.WeatherManager.m_targetDirection = this.weatherState.WindDirection.Target;
        this.WeatherManager.m_directionSpeed = this.weatherState.WindDirection.Speed;
      }
    }

    private void HandlePrecipitationParticles()
    {
      if (DayNightProperties.instance != null)
      {
        RainParticleProperties rainParticleProperties = DayNightProperties.instance.GetComponent<RainParticleProperties>();
        if (rainParticleProperties != null)
        {
          rainParticleProperties.SetFieldValue("m_IsWinter", this.CurrentWeatherProperties.m_rainIsSnow);

          if (this.CurrentWeatherProperties.m_rainIsSnow)
          {
            rainParticleProperties.m_RainMaterialX.EnableKeyword("SNOWPARTICLE");
            rainParticleProperties.m_RainMaterialX.DisableKeyword("RAINPARTICLE");
            rainParticleProperties.m_RainMaterialY.EnableKeyword("SNOWPARTICLE");
            rainParticleProperties.m_RainMaterialY.DisableKeyword("RAINPARTICLE");
            rainParticleProperties.m_RainMaterialZ.EnableKeyword("SNOWPARTICLE");
            rainParticleProperties.m_RainMaterialZ.DisableKeyword("RAINPARTICLE");

            if (rainParticleProperties.m_RainMaterialX.GetTexture("_RainNoise").name == "linearrainnoise")
            {
              try
              {
                // this still produce exception log :/
                rainParticleProperties.InvokeMethod("CreateNoiseSum");
              }
              catch { }
            }
          }
          else
          {
            rainParticleProperties.m_RainMaterialX.EnableKeyword("RAINPARTICLE");
            rainParticleProperties.m_RainMaterialX.DisableKeyword("SNOWPARTICLE");
            rainParticleProperties.m_RainMaterialY.EnableKeyword("RAINPARTICLE");
            rainParticleProperties.m_RainMaterialY.DisableKeyword("SNOWPARTICLE");
            rainParticleProperties.m_RainMaterialZ.EnableKeyword("RAINPARTICLE");
            rainParticleProperties.m_RainMaterialZ.DisableKeyword("SNOWPARTICLE");

            Texture2D sumNoise = rainParticleProperties.GetFieldValue("m_SumNoiseTexture") as Texture2D;
            if (sumNoise != null)
            {
              rainParticleProperties.SetFieldValue("m_SumNoiseTexture", null);
              UnityEngine.Object.DestroyImmediate(sumNoise);
            }
          }
          //rainParticleProperties.InvokeMethod("OnDisable");
          //rainParticleProperties.InvokeMethod("OnEnable");
        }
      }
    }

    private void HandleSnowDumps(TimeSpan timeDelta)
    {
      float tempDeltaFall = this.WeatherManager.m_currentTemperature - this.ClimateControlProperties.SnowFallTemperature;
      float tempDeltaMelt = this.WeatherManager.m_currentTemperature - this.ClimateControlProperties.SnowMeltTemperature;

      float snowMeltRatio = 1f;

      if (tempDeltaFall > 0)
        snowMeltRatio *= (1 + tempDeltaFall / 90);
      else // add extra snow?
        snowMeltRatio = 0;

      if (tempDeltaMelt > 0)
        snowMeltRatio *= (1 + tempDeltaMelt / 30);

      if (snowMeltRatio > 1)
        snowMeltRatio = (float)Math.Pow(snowMeltRatio, timeDelta.TotalHours);

      FastList<ushort> serviceBuildings = this.buildingManager.GetServiceBuildings(ItemClass.Service.Road);
      int num = serviceBuildings.m_size;
      ushort[] numArray = serviceBuildings.m_buffer;
      if (numArray != null && num <= numArray.Length)
      {
        for (int index = 0; index < num; ++index)
        {
          ushort buildingId = numArray[index];
          if ((int)buildingId != 0)
          {
            BuildingInfo info = this.buildingManager.m_buildings.m_buffer[(int)buildingId].Info;
            if (info.m_class.m_service == ItemClass.Service.Road)
            {
              SnowDumpAI snowDump = info.m_buildingAI as SnowDumpAI;
              if (snowDump != null)
              {
                int snowInDump = (int)this.buildingManager.m_buildings.m_buffer[(int)buildingId].m_customBuffer1 * 1000 + (int)this.buildingManager.m_buildings.m_buffer[(int)buildingId].m_garbageBuffer; // snowDump.GetSnowAmount(buildingId, ref bm.m_buildings.m_buffer[(int)buildingId]);
                int newSnowAmount = snowInDump;

                int rainOnArea = 0;
                //int rainOnArea = area * this.weatherManager.m_currentRain;

                if (snowMeltRatio > 1)
                {
                  int productionRate = (int)((snowMeltRatio /*- 1f*/) * 100f);

                  int a = (int)this.buildingManager.m_buildings.m_buffer[(int)buildingId].m_customBuffer1 * 1000 + (int)this.buildingManager.m_buildings.m_buffer[(int)buildingId].m_garbageBuffer;
                  int meltedSnow = Mathf.Min(a, (productionRate * snowDump.m_snowConsumption + 99) / 100);

                  if (meltedSnow > 0)
                  {
                    newSnowAmount = snowInDump - meltedSnow;
                    meltedSnow = meltedSnow + rainOnArea; // rain is washing that dirty snow and pollute your land!
                    Singleton<NaturalResourceManager>.instance.TryDumpResource(NaturalResourceManager.Resource.Pollution, meltedSnow * 100, meltedSnow * 100, this.buildingManager.m_buildings.m_buffer[(int)buildingId].m_position, snowDump.m_pollutionRadius);
                  }

                  newSnowAmount = newSnowAmount - rainOnArea;
                }
                else
                {
                  newSnowAmount = snowInDump + rainOnArea;
                }

                newSnowAmount = Math.Min(Math.Max(newSnowAmount, 0), snowDump.m_snowCapacity);

                // update snow in dump with new values if they have changed!
                if (snowInDump != newSnowAmount)
                {
                  this.buildingManager.m_buildings.m_buffer[(int)buildingId].m_customBuffer1 = (ushort)(newSnowAmount / 1000);
                  this.buildingManager.m_buildings.m_buffer[(int)buildingId].m_garbageBuffer = (ushort)(newSnowAmount - (int)this.buildingManager.m_buildings.m_buffer[(int)buildingId].m_customBuffer1 * 1000);
                }

                // snow amount
                //return Mathf.Min(this.m_snowCapacity, (int)data.m_customBuffer1 * 1000 + (int)data.m_garbageBuffer);

                // snow reduction per tick
                //int a = (int)buildingData.m_customBuffer1 * 1000 + (int)buildingData.m_garbageBuffer;
                //int num1 = Mathf.Min(a, (productionRate * this.m_snowConsumption + 99) / 100);
                //if (num1 > 0)
                //{
                //  a -= num1;
                //  buildingData.m_customBuffer1 = (ushort)(a / 1000);
                //  buildingData.m_garbageBuffer = (ushort)(a - (int)buildingData.m_customBuffer1 * 1000);
                //}

                // polution per tick
                //if ((int)buildingData.m_customBuffer1 >= 100)
                //{
                //  int num = ((int)buildingData.m_customBuffer1 / 100 * this.m_pollutionAccumulation + 99) / 100;
                //  if (num != 0)
                //    Singleton<NaturalResourceManager>.instance.TryDumpResource(NaturalResourceManager.Resource.Pollution, num, num, buildingData.m_position, this.m_pollutionRadius);
                //}
              }
            }
          }
        }
      }
    }

    private void HandleRainfallMakesWater(TimeSpan timeDelta)
    {
      if (this.CurrentRain > 0.25f && this.CurrentGroundWetness > 0.5f)
      {
        TerrainPatch randomPatch1 = this.terrainManager.m_patches[this.random.Next(0, this.terrainManager.m_patches.Length - 1)];
        TerrainPatch randomPatch2 = this.terrainManager.m_patches[this.random.Next(0, this.terrainManager.m_patches.Length - 1)];
        TerrainPatch randomPatch3 = this.terrainManager.m_patches[this.random.Next(0, this.terrainManager.m_patches.Length - 1)];
        TerrainPatch randomPatch4 = this.terrainManager.m_patches[this.random.Next(0, this.terrainManager.m_patches.Length - 1)];

        float waterToAcquire = Mathf.Clamp((this.CurrentGroundWetness - 0.75f + this.CurrentRain - 0.25f) * 65535f, 0, 65535);

        float acquiredWater = this.AcquireSurfaceWater(waterToAcquire);

        if (acquiredWater > 0)
        {
          ushort waterPerPatch = (ushort)(acquiredWater * 0.25f);
          float waterAdded = 0;
          waterAdded += GeneralHelper.TryAddWater(randomPatch1.m_terrainPosition, waterPerPatch);
          waterAdded += GeneralHelper.TryAddWater(randomPatch2.m_terrainPosition, waterPerPatch);
          waterAdded += GeneralHelper.TryAddWater(randomPatch3.m_terrainPosition, waterPerPatch);
          waterAdded += GeneralHelper.TryAddWater(randomPatch4.m_terrainPosition, waterPerPatch);

          float waterNotAdded = acquiredWater - waterAdded;

          this.DisperseSurfaceWater(waterNotAdded);
        }
      }
    }

    private void HandleThundersFrequency(TimeSpan timeDelta)
    {
      if (this.ThundersFrequency == Vector2.zero)
        return;

      float roll = (float)(this.random.NextDouble() * 50);

      if (roll > timeDelta.TotalMinutes)
        return;

      if (this.ThundersFrequency.x > 0)
      {
        roll = (float)(this.random.NextDouble() * 100);

        if (roll < this.ThundersFrequency.x)
        {
          FastList<WeatherManager.LightningStrike> m_lightningQueue = (FastList<WeatherManager.LightningStrike>)this.WeatherManager.GetFieldValue("m_lightningQueue");

          for (int i = 0; i < m_lightningQueue.m_size; i++)
          {
            uint startFrame = m_lightningQueue.m_buffer[i].m_startFrame;
            if (startFrame != 0u)
            {
              InstanceID empty = InstanceID.Empty;
              empty.Lightning = (byte)i;
              Singleton<InstanceManager>.instance.ReleaseInstance(empty);
              m_lightningQueue.m_buffer[i].m_startFrame = 0u;
            }
          }
          while (m_lightningQueue.m_size > 0 && m_lightningQueue.m_buffer[m_lightningQueue.m_size - 1].m_startFrame == 0u)
          {
            m_lightningQueue.m_size--;
          }
        }
      }

      if (this.CurrentRain > 0.5f && this.ThundersFrequency.y > 0)
      {
        roll = (float)(this.random.NextDouble() * 100);

        if (roll < this.ThundersFrequency.y)
        {
          this.WeatherManager.QueueLightningStrike((uint)roll);
        }
      }
    }

    private void HandleWaterSourcesModule(TimeSpan timeDelta)
    {
      if (modulesHandling.ContainsKey(ClimateControlModuleTypeIdentifiers.WaterSource))
        return;

      float totalFlow = 0;
      FastList<WaterSource> waterSources = this.terrainManager.WaterSimulation.m_waterSources;
      for (int i = 0; i < waterSources.m_size; i++)
      {
        if (waterSources.m_buffer[i].m_type == WaterSource.TYPE_NATURAL)
        {
          totalFlow += waterSources.m_buffer[i].m_flow;
        }
      }

      float toAcquire = totalFlow * 0.33f;
      while (totalFlow > 1000)
      {
        bool anyAcquired = false;

        float acquired = this.AcquireOutOfMapGroundWater(toAcquire);
        if (acquired > 0)
          anyAcquired = true;
        totalFlow -= acquired;
        toAcquire = Math.Min(toAcquire, totalFlow);

        acquired = this.AcquireSurfaceWater(toAcquire);
        if (acquired > 0)
          anyAcquired = true;
        totalFlow -= acquired;
        toAcquire = Math.Min(toAcquire, totalFlow);

        acquired = this.AcquireGroundWater(toAcquire);
        if (acquired > 0)
          anyAcquired = true;
        totalFlow -= acquired;
        toAcquire = Math.Min(toAcquire, totalFlow);

        if (anyAcquired == false)
          break;
      }
    }
  }
}